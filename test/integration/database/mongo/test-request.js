/**
 * User: curtis
 * Date: 9/22/18
 * Time: 1:13 PM
 * Copyright @2018 by Xraymen Inc.
 */

const mongodb=require("mongodb");
const assert=require("pig-core").assert;
const test_factory=require("../../../support/factory");
const proxy=require("pig-core").proxy;
const api=require("../../../../lib/index");
const file=require("pig-core").file;

/**
 * @summary: A hybrid of integration and unit testing meaning that we send mock requests to our framework
 * 	but intercept the db requests and make sure the request is what we are expecting.
 */
describe("services.database.request.mongo", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
		return test_factory.reset();
	});

	it("count should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "countDocuments", (filter)=>{
					assert.deepEqual(filter, {
						"type": "boy"
					});
					return Promise.resolve(1);
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-count.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.strictEqual(response.result, 1);
			});
	});

	it("create should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "insertMany", (docs, options)=>{
					assert.deepEqual(docs, [
						{
							"address": {
								"zip": 11377,
								"city": "New York City"
							},
							"name": "George",
							"type": "boy"
						}
					]);
					assert.deepEqual(options, {});
					return Promise.resolve([]);
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-create.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.strictEqual(response.result, null);
			});
	});

	it("creates should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "insertMany", (docs, options)=>{
					assert.deepEqual(docs, [
						{
							"address": {
								"zip": 11377,
								"city": "Sunnyside"
							},
							"name": "George",
							"type": "boy"
						},
						{
							"address": {
								"zip": 11388,
								"city": "Woodside"
							},
							"name": "Daisy",
							"type": "girl"
						}
					]);
					assert.deepEqual(options, {
						"projection": {
							"_id": 1,
							"address.city": 1,
							"address.zip": 1,
							"name": 1,
							"type": 1
						}
					});
					return Promise.resolve([]);
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-creates.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, []);
			});
	});

	it("deleteByZip should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "deleteOne", (query, options)=>{
					assert.deepEqual(query, {
						"address.zip": 11377
					});
					assert.deepEqual(options, {});
					return Promise.resolve({
						deletedCount: 1
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-deleteByZip.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, {deleted: 1});
			});
	});

	it("distinct should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "distinct", (field, filter)=>{
					assert.strictEqual(field, "address.city");
					assert.deepEqual(filter, {});
					return Promise.resolve(["New York"]);
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-distinctCities.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, ["New York"]);
			});
	});

	it("getById should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "find", (query, options)=>{
					// the test id is invalid so we get creative
					assert.deepEqual(Object.keys(query), ["_id"]);
					assert.deepEqual(options, {
						"projection": {
							"_id": 1,
							"address.city": 1,
							"address.zip": 1,
							"name": 1,
							"type": 1
						}
					});
					return {
						toArray: ()=>Promise.resolve([])
					};
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-getById.json"));
			})
			.catch(error=>{
				assert.strictEqual(error.statusCode, 404);
			});
	});

	it("getByZip should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-mongo-setup.json"
		})
			.then(()=>{
				proxy.stub(mongodb.Collection.prototype, "find", (query, options)=>{
					assert.deepEqual(query, {
						"address.zip": {
							$gte: 11377
						}
					});
					assert.deepEqual(options, {
						"projection": {
							"address.city": 1,
							"address.zip": 1
						},
						"limit": 50
					});
					return {
						toArray: ()=>Promise.resolve([])
					};
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-getByZip.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, []);
			});
	});
});
