/**
 * User: curtis
 * Date: 9/22/18
 * Time: 1:13 PM
 * Copyright @2018 by Xraymen Inc.
 */

const pg=require("pg");
const assert=require("pig-core").assert;
const test_factory=require("../../../support/factory");
const proxy=require("pig-core").proxy;
const api=require("../../../../lib/index");
const file=require("pig-core").file;

/**
 * @summary: A hybrid of integration and unit testing meaning that we send mock requests to our framework
 * 	but intercept the db requests and make sure the request is what we are expecting.
 */
describe("services.database.request.pgsql", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
		return test_factory.reset();
	});

	it("count should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, "SELECT 1 FROM \"urn:db:mongo:col:By1EjVlQX\" WHERE \"type\"=$1");
					assert.deepEqual(params, ["boy"]);
					done(null, {
						rowCount: 1
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-count.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.strictEqual(response.result, 1);
			});
	});

	it("create should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'INSERT INTO "urn:db:mongo:col:By1EjVlQX" ("name","type","address.city","address.zip") VALUES ($1,$2,$3,$4)');
					assert.deepEqual(params, [
						"George",
						"boy",
						"New York City",
						11377
					]);
					done(null, {
						rows: []
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-create.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.strictEqual(response.result, null);
			});
	});

	it("creates should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'INSERT INTO "urn:db:mongo:col:By1EjVlQX" ("name","type","address.city","address.zip") ' +
						'VALUES ($1,$2,$3,$4), ($5,$6,$7,$8) RETURNING "_id","address.city","address.zip","name","type"');
					assert.deepEqual(params, [
						"George", "boy", "Sunnyside", 11377,
						"Daisy", "girl", "Woodside", 11388
					]);
					done(null, {
						rows: []
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-creates.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, null);
			});
	});

	it("deleteByZip should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'DELETE FROM "urn:db:mongo:col:By1EjVlQX" WHERE "address.zip"=$1');
					assert.deepEqual(params, [11377]);
					done(null, {
						rowCount: 0
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-deleteByZip.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, {deleted: 0});
			});
	});

	it("distinct should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'SELECT DISTINCT "address.city" FROM "urn:db:mongo:col:By1EjVlQX"');
					done(null, {
						rows: [
							{"address.city": "New York"}
						]
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-distinctCities.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, ["New York"]);
			});
	});

	it("getById should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'SELECT "_id","address.city","address.zip","name","type" FROM "urn:db:mongo:col:By1EjVlQX" WHERE "_id"=$1');
					assert.deepEqual(params, [1]);
					done(null, {
						rows: []
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-getById.json"));
			})
			.catch(error=>{
				assert.strictEqual(error.statusCode, 404);
			});
	});

	it("getByZip should send a properly constructed query and params", function() {
		return test_factory.initializeAPI({
			specPath: "test/res/data/pig/spec-pgsql-setup.json"
		})
			.then(()=>{
				// note: the reason we treat this as a callback vs. a promise is because of the library we use and how
				// he routes pool requests to minion clients which are treated as callbacks and not promises.
				proxy.stubCallback(pg.Client.prototype, "query", (query, params, done)=>{
					assert.strictEqual(query, 'SELECT "address.city","address.zip" FROM "urn:db:mongo:col:By1EjVlQX" WHERE "address.zip">=$1 LIMIT 50');
					assert.deepEqual(params, [11377]);
					done(null, {
						rows: []
					});
				});
				return api.route.process(file.readToJSONSync("./test/res/data/pig/user/spec-request-getByZip.json"));
			})
			.then(response=>{
				assert.strictEqual(response.statusCode, 200);
				assert.deepEqual(response.result, []);
			});
	});
});
