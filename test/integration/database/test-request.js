/**
 * User: curtis
 * Date: 9/22/18
 * Time: 1:13 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const file=require("pig-core").file;
const util=require("pig-core").util;
const test_factory=require("../../support/factory");
const proxy=require("pig-core").proxy;
const api=require("../../../lib/index");

const targets=[
	{
		disabled: false,
		name: "mongo",
		specPath: "test/res/data/pig/spec-mongo-setup.json"
	},
	{
		disabled: false,
		name: "postgreSQL",
		specPath: "test/res/data/pig/spec-pgsql-setup.json"
	}
];

/**
 * @summary: Runs our mock requests and examines their results.
 * @goal: make sure that there is parity across our db services.
 */
describe("services.database.request", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
		return test_factory.reset();
	});

	targets
		.filter(target=>!target.disabled)
		.forEach(target=>{
			/**
			 * Runs the sequence:
			 * - initializes environment with <code>target.specPath</code>
			 * - runs batch requests to setup db.
			 * - runs specified request
			 * @param {string} batchPath
			 * @param {string} requestPath
			 * @return {Promise<*>} - returns results of <code>route.process</code>
			 */
			function _executeRequest({batchPath, requestPath}) {
				const request=file.readToJSONSync(requestPath);
				return test_factory.initializeAPI({specPath: target.specPath})
					.then(test_factory.runRequestSequence.bind(null, batchPath))
					.then(api.route.process.bind(null, request));
			}

			it(`${target.name}.count should respond with proper count`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#create-user",
					requestPath: "./test/res/data/pig/user/spec-request-count.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					assert.equal(response.result, 1);
				});
			});

			it(`${target.name}.create should create a single user and respond properly`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#clear-user",
					requestPath: "./test/res/data/pig/user/spec-request-create.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					// note: "spec-request-create.json" does not want response data
					assert.strictEqual(response.result, null);
				});
			});

			it(`${target.name}.creates should create multiple users and respond properly`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#clear-user",
					requestPath: "./test/res/data/pig/user/spec-request-creates.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					assert.deepEqual(util.arrayOmit(response.result, "_id"), [
						{
							"address": {
								"city": "Sunnyside",
								"zip": 11377
							},
							"name": "George",
							"type": "boy"
						},
						{
							"address": {
								"city": "Woodside",
								"zip": 11388
							},
							"name": "Daisy",
							"type": "girl"
						}
					]);
				});
			});

			it(`${target.name}.deleteByZip should delete 1 user and respond properly`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#create-user",
					requestPath: "./test/res/data/pig/user/spec-request-deleteByZip.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					assert.deepEqual(response.result, {deleted: 1});
				});
			});

			it(`${target.name}.distinct should properly pull back all unique city and respond properly`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#create-users",
					requestPath: "./test/res/data/pig/user/spec-request-distinctCities.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					assert.deepEqual(response.result, [
						"Sunnyside",
						"Woodside"
					]);
				});
			});

			it(`${target.name}.getByZip should find user and respond properly`, function() {
				return _executeRequest({
					batchPath: "./test/res/data/pig/batch-requests.json#create-user",
					requestPath: "./test/res/data/pig/user/spec-request-getByZip.json"
				}).then(response=>{
					assert.strictEqual(response.statusCode, 200);
					// why are we removing _id before the comparison? Because mongo is naughty and returns it even if you don't ask for it.
					assert.deepEqual(util.arrayOmit(response.result, "_id"), [
						{
							"address": {
								"city": "New York City",
								"zip": 11377
							}
						}
					]);
				});
			});
		});
});
