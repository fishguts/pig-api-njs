/**
 * User: curtis
 * Date: 2/14/18
 * Time: 1:01 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const test_factory=require("../../support/factory");
const {Specification}=require("../../../lib/model/specification");

const targets=[
	{
		disabled: false,
		name: "mongo",
		specPath: "test/res/data/pig/spec-mongo-setup.json"
	},
	{
		disabled: false,
		name: "postgreSQL",
		specPath: "test/res/data/pig/spec-pgsql-setup.json"
	}
];

describe("services.database.setup", function() {
	afterEach(()=>{
		return test_factory.reset();
	});

	targets
		.filter(target=>!target.disabled)
		.forEach(target=>{
			it(`should initialize and setup ${target.name} properly`, function() {
				return test_factory.initializeAPI({
					specPath: target.specPath
				})
					.then(function(specification) {
						assert.ok(specification instanceof Specification);
					});
			});
		});
});
