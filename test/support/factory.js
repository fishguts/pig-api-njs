/**
 * User: curtis
 * Date: 7/21/18
 * Time: 9:37 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const api=require("../../lib/index");
const file=require("pig-core").file;
const promise=require("pig-core").promise;
const factory=require("../../lib/model/factory");
const {Specification}=require("../../lib/model/specification");

/**
 * Creates a Specification out of a file based spec.
 * @param {string} path
 * @returns {Specification}
 */
exports.createSpecification=function(path=file.relativePathToAbsolute({
	module,
	relativeToModuleRoot: "test/res/data/pig/user/spec-mongo.json"
})) {
	return new Specification(file.readToJSONSync(path));
};

/**
 * Creates an IncommingRequest
 * @param {string} path - of mock request
 * @returns {IncommingRequest}
 */
exports.createIncommingRequest=function({
	requestPath=file.relativePathToAbsolute({
		module,
		relativeToModuleRoot: "test/res/data/pig/user/spec-request-getById.json"
	}),
	specPath=file.relativePathToAbsolute({
		module,
		relativeToModuleRoot: "test/res/data/pig/user/spec-mongo.json"
	})
}={}) {
	const spec=exports.createSpecification(specPath),
		mock=file.readToJSONSync(requestPath);
	return factory.incommingRequest.create(mock, spec);
};

/**
 * Sets up our environment with the specified (or defaulted) spec.
 * @param {string} specPath
 * @returns {Promise<Specification>}
 */
exports.initializeAPI=function({
	specPath=file.relativePathToAbsolute({
		module,
		relativeToModuleRoot: "test/res/data/pig/user/spec-mongo.json"
	})
}={}) {
	return api.initialize(specPath)
		.catch((error)=>{
			assert.ok(false, `attempt to initialize with path='${specPath}' failed: ${error}`);
		});
};

/**
 * Resets properties of the environment that are setup when a specification is injected
 * @returns {Promise<void>}
 */
exports.reset=function() {
	factory.specification.reset();
	return api.shutdown();
};

/**
 * Runs a request sequence. Assumes that the environment has already been initialized.
 * @param {string} path - to batch or sequence with optional property at the end: <path>[#sequenceName]
 * @returns {Promise<void>}
 */
exports.runRequestSequence=async function(path) {
	const parts=path.split("#"),
		sequences=file.readToJSONSync(parts[0]),
		sequence=parts.length>1
			? sequences[parts[1]].sequence
			: sequences;

	return promise.series(sequence.map(item=>{
		// we only are handling requests as paths right now. Should we want to extend this functionality
		// then this is the place to do it.
		const request=file.readToJSONSync(item.requestPath);
		return api.route.process.bind(null, request);
	}));
};
