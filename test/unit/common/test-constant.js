/**
 * User: curtis
 * Date: 11/14/18
 * Time: 11:52 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const constant=require("../../../lib/common/constant");

describe("common.constant", function() {
	describe("isValidApiServiceClass", function() {
		it("should pass a valid value", function() {
			assert.strictEqual(constant.isValidApiServiceClass(constant.api.service.class.DATABASE), true);
		});

		it("should fail an invalid value", function() {
			assert.strictEqual(constant.isValidApiServiceClass("invalid"), false);
		});
	});

	describe("isValidApiTypeClass", function() {
		it("should pass a valid value", function() {
			assert.strictEqual(constant.isValidApiTypeClass(constant.api.service.type.HTTP), true);
		});

		it("should fail an invalid value", function() {
			assert.strictEqual(constant.isValidApiTypeClass("invalid"), false);
		});
	});

	describe("isValidEvent", function() {
		it("should pass a valid value", function() {
			assert.strictEqual(constant.isValidEvent(constant.event.SETTINGS_LOADED), true);
		});

		it("should fail an invalid value", function() {
			assert.strictEqual(constant.isValidEvent("invalid"), false);
		});
	});
});
