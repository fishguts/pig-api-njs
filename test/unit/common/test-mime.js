/**
 * User: curtis
 * Date: 3/1/18
 * Time: 11:56 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const mime=require("../../../lib/common/mime");

describe("common.mime", function() {
	describe("parse", function() {
		it("should return null if param is empty", function() {
			assert.equal(mime.parse(null), null);
		});

		it("should type alone if no encoding", function() {
			assert.deepEqual(mime.parse("test"), {
				type: "test"
			});
		});

		it("should also return charset if included", function() {
			assert.deepEqual(mime.parse("test;charset=utf-8"), {
				type: "test",
				charset: "utf-8"
			});
			assert.deepEqual(mime.parse("test ; charset = utf-8"), {
				type: "test",
				charset: "utf-8"
			});
		});
	});
});
