/**
 * User: curtis
 * Date: 9/6/18
 * Time: 9:01 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const proxy=require("pig-core").proxy;
const constant=require("../../../../../lib/common/constant");
const _sql=require("../../../../../lib/services/database/_sql/index");

describe("services.database._sql", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.unstub();
		proxy.log.unstub();
	});

	describe("fragment.fieldToColumnAttributes", function() {
		it("should return empty string if no attributes", function() {
			assert.strictEqual(_sql.fragment.fieldToColumnAttributes({}), "");
			assert.strictEqual(_sql.fragment.fieldToColumnAttributes({
				attributes: []
			}), "");
		});

		it("should return PRIMARY KEY if field is auto-id", function() {
			assert.strictEqual(_sql.fragment.fieldToColumnAttributes({
				attributes: [constant.field.attribute.AUTO_ID]
			}), "PRIMARY KEY");
		});

		it("should return NOT NULL if field is required", function() {
			assert.strictEqual(_sql.fragment.fieldToColumnAttributes({
				attributes: [constant.field.attribute.REQUIRED]
			}), "NOT NULL");
		});

		it("should properly combine attributes", function() {
			assert.strictEqual(_sql.fragment.fieldToColumnAttributes({
				attributes: [
					constant.field.attribute.AUTO_ID,
					constant.field.attribute.REQUIRED
				]
			}), "PRIMARY KEY NOT NULL");
		});
	});

	describe("fragment.fieldToColumnType", function() {
		[
			[constant.field.type.BOOLEAN, "BOOLEAN"],
			[constant.field.type.DATE, "TIMESTAMP"],
			[constant.field.type.ENUM, "TEXT"],
			[constant.field.type.ID, "BIGINT"],
			[constant.field.type.INTEGER, "BIGINT"],
			[constant.field.type.NUMBER, "DOUBLE"],
			[constant.field.type.OBJECT, "JSON"],
			[constant.field.type.STRING, "TEXT"]
		].forEach(types=>{
			it(`should convert ${types[0]} properly`, function() {
				assert.strictEqual(_sql.fragment.fieldToColumnType({
					type: types[0]
				}), types[1]);
			});
		});

		it("should translate default type to serial type if is auto-type field", function() {
			const field={
				attributes: [constant.field.attribute.AUTO_ID],
				type: constant.field.type.INTEGER
			};
			assert.strictEqual(_sql.fragment.fieldToColumnType(field), "BIGSERIAL");
		});
	});

	describe("value.escape", function() {
		it("should return NULL if null or undefined", function() {
			assert.strictEqual(_sql.value.escape(undefined), "NULL");
			assert.strictEqual(_sql.value.escape(null), "NULL");
		});

		it("when not using dollar quoting it should return doubly quoted single quotes", function() {
			assert.strictEqual(_sql.value.escape("a'b'c"), "a''b''c");
		});

		it("when using dollar quoting it should return newly quoted string", function() {
			assert.strictEqual(_sql.value.escape("abc", {dollar: true}), "$pig$abc$pig$");
			assert.strictEqual(_sql.value.escape("abc", {dollar: true, quote: "$$"}), "$$abc$$");
		});
	});
});
