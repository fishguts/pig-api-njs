/**
 * User: curtis
 * Date: 2/14/18
 * Time: 1:01 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const index=require("../../lib/index");
const proxy=require("pig-core").proxy;

describe("index", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("route", function() {
		// todo: hijack this guy. It is too hard to not have the spec configured
		it("process return a rejection if specification is not configured", function(done) {
			index.route.process()
				.catch((error)=>{
					assert.ok(error instanceof Error);
					done();
				});
		});
	});
});
