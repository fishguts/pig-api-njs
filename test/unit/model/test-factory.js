/**
 * User: curtis
 * Date: 11/11/17
 * Time: 4:09 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const proxy=require("pig-core").proxy;
const test_factory=require("../../support/factory");
const file=require("pig-core").file;
const constant=require("../../../lib/common/constant");
let factory=require("../../../lib/model/factory");
const {Specification}=require("../../../lib/model/specification");


describe("model.factory", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
		proxy.unstub();

		// we want to be able to test our caching so we unload and reload him after each test
		delete require.cache[require.resolve("../../../lib/model/factory")];
		factory=require("../../../lib/model/factory");
	});

	describe("application", function() {
		it("should create a configuration based on specified environment", function() {
			_.forIn(constant.nodenv, function(value) {
				const application=factory.application.get(value);
				assert.equal(application.nodenv, value);
			});
		});
	});

	describe("gamuts", function() {
		it("should load if not loaded", function() {
			proxy.spy(file, "readToJSONSync");
			const gamuts=factory.gamuts.get();
			assert.ok(_.isArray(gamuts.log.level));
			assert.strictEqual(file.readToJSONSync.callCount, 1);
		});

		it("should not load if already loaded", function() {
			proxy.spy(file, "readToJSONSync");
			factory.gamuts.get();
			const gamuts=factory.gamuts.get();
			assert.ok(_.isArray(gamuts.log.level));
			assert.strictEqual(file.readToJSONSync.callCount, 1);
		});
	});

	describe("incommingRequest", function() {
		it("should throw exception if application wide specification is null", function() {
			factory.specification.reset();
			assert.throws(factory.incommingRequest.create.bind(factory.incommingRequest, {}, null));
		});

		it("should properly create a request with valid input", function() {
			const request=test_factory.createIncommingRequest();
			assert.ok(request.specification instanceof Specification);
			assert.notEqual(request.extensions, null);
		});
	});

	describe("specification", function() {
		it("should properly create a specification from a path", function() {
			const specification=file.relativePathToAbsolute({
				module,
				relativeToModuleRoot: "test/res/data/pig/user/spec-mongo.json"
			});
			assert.doesNotThrow(factory.specification.set.bind(factory.specification, specification));
			assert.ok(factory.specification.get() instanceof Specification);
		});
	});
});
