/**
 * User: curtis
 * Date: 2/17/18
 * Time: 9:51 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const proxy=require("pig-core").proxy;
const test_factory=require("../../support/factory");
const file=require("pig-core").file;
const {Specification}=require("../../../lib/model/specification");
const {IncommingRequest}=require("../../../lib/model/request");

describe("data.IncommingRequest", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("constructor", function() {
		it("should construct properly", function() {
			const instance=test_factory.createIncommingRequest();
			assert.ok(instance instanceof IncommingRequest);
			assert.ok(instance.specification instanceof Specification);
			assert.ok(_.isPlainObject(instance.operation));
			assert.ok(_.isPlainObject(instance.getParamSchema("_id")));
			assert.equal(instance.getBodySchema(), undefined);
			assert.ok(_.isObject(instance.stack));
		});
	});

	describe("stack", function() {
		describe("constructor", function() {
			it("should construct params properly", function() {
				const instance=test_factory.createIncommingRequest(),
					stack=instance.stack;
				assert.equal(stack.body, undefined);
				assert.deepEqual(stack.params, {_id: 1});
				assert.deepEqual(stack.result, undefined);
				assert.equal(stack.statusCode, 200);
			});
		});

		describe("body", function() {
			it("should properly find body param", function() {
				const instance=test_factory.createIncommingRequest({
					requestPath: file.relativePathToAbsolute({
						module,
						relativeToModuleRoot: "test/res/data/pig/user/spec-request-create.json"
					})
				});
				assert.ok(!_.isEmpty(instance.stack.body));
			});
		});

		describe("push", function() {
			it("should default unspecified properties properly", function() {
				[
					{params: {gorge: "george"}},
					{statusCode: 300},
					{result: {"junk": "food"}, contentType: "contentType"},
					{params: {gorge: "george"}, result: "result", contentType: "contentType", statusCode: 300}
				].forEach((override)=> {
					const instance=test_factory.createIncommingRequest(),
						stack=instance.stack;
					stack.push(override);
					assert.equal(stack._stack.length, 2);
					assert.equal(stack.body, _.get(override, "body"));
					assert.deepEqual(stack.params, _.get(override, "params", {_id: 1}));
					assert.equal(stack.result, _.get(override, "result"));
					assert.equal(stack.contentType, _.get(override, "contentType"));
					assert.equal(stack.statusCode, _.get(override, "statusCode", 200));
				});
			});
		});

		describe("get", function() {
			it("should properly get without cloning", function() {
				const instance=test_factory.createIncommingRequest(),
					stack=instance.stack,
					update={
						params: {gorge: "george"}
					};
				stack.push(update);
				assert.equal(stack.get(false).params, update.params);
			});

			it("should properly get with cloning", function() {
				const instance=test_factory.createIncommingRequest(),
					stack=instance.stack,
					update={
						params: {gorge: "george"}
					};
				stack.push(update);
				assert.notEqual(stack.get(true).params, update.params);
				assert.deepEqual(stack.get(true).params, update.params);
			});
		});
	});
});
