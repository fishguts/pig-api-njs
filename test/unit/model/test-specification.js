/**
 * User: curtis
 * Date: 2/14/18
 * Time: 12:28 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const proxy=require("pig-core").proxy;
const test_factory=require("../../support/factory");

describe("data.specification", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("constructor", function() {
		it("should construct properly with acceptable params", function() {
			const instance=test_factory.createSpecification();
			assert.equal(instance.raw.swagger, "2.0");
		});
	});

	describe("getDefinition", function() {
		it("should find existing defintion", function() {
			const instance=test_factory.createSpecification();
			assert.notEqual(instance.getDefinition("User"), undefined);
		});

		it("should return undefined if not found", function() {
			const instance=test_factory.createSpecification();
			assert.equal(instance.getDefinition("[fail]"), undefined);
		});
	});

	describe("getModel", function() {
		it("should should throw an exception if not found", function() {
			const instance=test_factory.createSpecification();
			assert.throws(()=>instance.getModel("not-found"));
		});

		it("should properly find model if it exists", function() {
			const instance=test_factory.createSpecification(),
				model=instance.getModel("urn:mdl:By1EjVlQX");
			assert.equal(model.id, "urn:mdl:By1EjVlQX");
		});
	});
});
