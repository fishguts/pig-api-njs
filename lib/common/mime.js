/**
 * User: curtis
 * Date: 3/1/18
 * Time: 11:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");

/**
 * Parses argument and returns the type and encoding
 * @param {string} mimeType
 * @returns {{type:String, charset:(String|undefined), boundary:(String|undefined)}}
 */
exports.parse=function(mimeType) {
	if(mimeType) {
		const parsed=mimeType.match(/\s*(\S+)\s*;\s*(charset|boundary)\s*=\s*(\S+)/i);
		if(parsed) {
			return _.set({type: parsed[1]}, parsed[2], parsed[3]);
		} else {
			return {
				type: mimeType
			};
		}
	}
	return mimeType;
};
