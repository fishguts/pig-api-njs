/**
 * User: curt
 * Date: 2/8/18
 * Time: 9:48 PM
 */

const _=require("lodash");
const constant=require("pig-core").constant;

/**
 * @type {module:pig-core/constant}
 */
module.exports=Object.assign(module.exports, constant);

/**
 * Constants having to do with our api
 */
exports.api={
	service: {
		class: {
			DATABASE: "database",
			SERVICE: "service"
		},
		type: {
			HTTP: "http",
			MONGO: "mongo",
			PGSQL: "pgsql",
			TRANSFORM: "transform",
			TEST_DB: "test-db"
		}
	}
};

/**
 * @type {function(value:string):Boolean}
 */
exports.isValidApiServiceClass=constant.isValidValue.bind(null, exports.api.service.class);
/**
 * @type {function(value:string):Boolean}
 */
exports.isValidApiTypeClass=constant.isValidValue.bind(null, exports.api.service.type);

/**
 * Enumerations that apply to fields
 */
exports.field={
	attribute: {
		AUTO_ID: "auto-id",
		MIXED: "mixed",
		REQUIRED: "required"
	},
	propertyOf: {
		// a field found in a model.
		MODEL: "model",
		// A type field that applies to queries.
		QUERY: "query",
		// Describes a response property that is not a field in the model such as "count" or "deleted"
		RESULT: "result"
	},
	type: {
		ARRAY: "array",
		BOOLEAN: "boolean",
		DATE: "date",
		ENUM: "enum",
		ID: "id",
		INTEGER: "integer",
		OBJECT: "object",
		NUMBER: "number",
		STRING: "string"
	}
};

/**
 * Known environments. The lib doesn't care too much. These just allow him to test and to establish a default
 */
exports.nodenv={
	TEST: "test",
	DEVELOPMENT: "development"
};
/**
 * @type {function(value:string):Boolean}
 */
exports.isValidNodenv=constant.isValidValue.bind(null, exports.nodenv);

/**
 * Some handy constants when working with openAPI spec
 */
exports.openapi={
	extensions: {
		PIG: "x-pig"
	}
};

/**
 * Constants related to requests and the attributes of a request.
 */
exports.request={
	param: {
		location: {
			BODY: "body",
			HEADER: "header",
			LITERAL: "literal",
			PATH: "path",
			QUERY: "query"
		},
		operation: {
			EQUAL: "eq",
			NOT_EQUAL: "ne",
			GREATER_THAN: "gt",
			GREATER_THAN_EQ: "gte",
			LESS_THAN: "lt",
			LESS_THAN_EQ: "lte",
			UPDATE: "set"
		},
		type: {
			/**
			 * Fields that filter responses
			 */
			FILTER: "filter",
			/**
			 * Fields that affect the query such as: limit, skip, sort
			 */
			QUERY: "query",
			/**
			 * Fields that carry data for updating the model
			 */
			UPDATE: "update"
		}
	}
};
