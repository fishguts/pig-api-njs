/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {Model}=require("./_base");

/**
 * Application information. Largely a server for our application configuration info with an application jobQueue.
 * @typedef {Model} Application
 */
class Application extends Model {
	/**
	 * Name of the project
	 */
	get name() { return this._data.info.name; }
	/**
	 * Description of the project
	 */
	get desc() { return this._data.info.description; }

	/**** Application Environment Information ****/
	/**
	 *
	 * @returns {{debug:boolean, isLower:boolean, description:String, name:String, variables:Object}}
	 */
	get environment() { return this._data.environment; }

	/**
	 * NODE_ENV or our own default
	 * @returns {string}
	 */
	get nodenv() { return this._data.environment.name; }

	/**
	 * Whether the current env should include debugging code or not
	 * @returns {boolean}
	 */
	get debug() { return this._data.environment.debug; }

	/**
	 * Log configuration information
	 * @returns {{level:String, transports:[{type:String, overrides:Object}]}}
	 */
	get log() { return this._data.log; }
}

exports.Application=Application;
