/**
 * User: curtis
 * Date: 2/14/18
 * Time: 1:18 PM
 * Copyright @2018 by Xraymen Inc.
 */

/**
 * Base class. All work with blob of data.
 */
class Model {
	/**
	 * @param {Object} data
	 */
	constructor(data) {
		this._data=data;
	}

	/**
	 * Gets the raw object that is this object
	 * @returns {Object}
	 */
	get raw() {
		return this._data;
	}
}

exports.Model=Model;
