/**
 * User: curtis
 * Date: 7/19/18
 * Time: 11:11 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * Collection of types that are schema definitions in JSDoc notation.
 */

/**************** Database Types ****************/
/**
 * Describes distilled properties of a database. Derived from a database's definition in our spec.
 * @typedef {Object} DatabaseAttributes
 * @property {undefined|{value: Array<string>, param: SpecParameter|undefined}} fields
 * @property {{value: Number, param: SpecParameter}} limit
 * @property {{value: Number, param: SpecParameter}} skip
 * @property {undefined|{value: Array<string>, param: SpecParameter}} sort
 */

/**
 * A single database filter as used by database services
 * @typedef {Object} DatabaseFilter
 * @property {string} operation
 * @property {SpecParameter} param
 * @property {string} path
 * @property {*} value
 */

/**
 * Array of database filters
 * @typedef {Array<DatabaseFilter>} DatabaseFilters
 */

/**
 * Database update type
 * @typedef {Object} DatabaseData
 * @property {Object|Array<Object>} value
 * @property {Array<SpecParameter>} params
 */

/**
 * Return type of <code>DatabaseCollection.count</code>
 * @typedef {Number} DatabaseCountResult
 */

/**
 * Return type of <code>DatabaseCollection.createOne</code>
 * @typedef {Object|null} DatabaseCreateOneResult
 */

/**
 * Return type of <code>DatabaseCollection.createMany</code>
 * @typedef {Array<Object>|null} DatabaseCreateManyResult
 */

/**
 * Return type of <code>DatabaseCollection.deleteOne</code>
 * @typedef {{deleted:Number}} DatabaseDeleteOneResult
 */

/**
 * Return type of <code>DatabaseCollection.deleteMany</code>
 * @typedef {{deleted:Number}} DatabaseDeleteManyResult
 */

/**
 * Return type of <code>DatabaseCollection.distinct</code>
 * @typedef {Array<*>} DatabaseDistinctResult
 */

/**
 * Return type of <code>DatabaseCollection.findOne</code>
 * @typedef {Object|null} DatabaseFindOneResult
 */

/**
 * Return type of <code>DatabaseCollection.findMany</code>
 * @typedef {Array<Object>} DatabaseFindManyResult
 */

/**
 * Return type of <code>DatabaseCollection.updateOne</code>
 * @typedef {Object|{updated:Number}} DatabaseUpdateOneResult
 */

/**
 * Return type of <code>DatabaseCollection.updateMany</code>
 * @typedef {Array<Object>|{updated:Number}} DatabaseUpdateManyResult
 */


/******************* Pig Types *******************/
/**
 * A type that is used predominantly where we want be able to plop lists in list/selection controls. But its use
 * extends beyond client code as we use this format in our <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {Array<{text:string, value:string}>} ControlItemList
 */

/**
 * Defaults for the various types we support may be encoded. This type is a list of all supported encodings
 * @typedef {"none"|"string"|"space-delimited"|"comma-delimited"|"semi-delimited"|"punctuation-delimited"} PigDefaultEncoding
 */

/**
 * All known pig types that a field type may be. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"array"|"boolean"|"date"|"enum"|"id"|"integer"|"object"|"number"|"string"} PigFieldType
 */

/**
 * All operations that may be assigned to a param. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"set"|"eq"|"ne"|"lt"|"lte"|"gt"|"gte"} PigOperation
 */

/**
 * All functions that may be assigned to a view. Definitive source is <link>res/configuration/model-gamuts.yaml</link>
 * @typedef {"count"|"create-one"|"create-many"|"delete-one"|"delete-many"|"distinct"|"get-one"|"get-many"|"update-one"} PigFunction
 */

/**
 * Description of our <link>res/configuration/model-gamuts.yaml</link.
 * @typedef {Object} PigGamuts
 * @property {Object} database
 * @property {ControlItemList} database.types
 * @property {Object} log
 * @property {ControlItemList} log.level
 * @property {Object} model
 * @property {Object} model.field
 * @property {ControlItemList} model.field.attributes
 * @property {ControlItemList} model.field.types
 * @property {Array<PigField>} model.fields
 * @property {Array<PigFieldReference|PigFieldOperation>} model.fieldReferences
 * @property {Array<ControlItemList>} model.operators
 * @property {Object<string, {name:string}>} model.transform
 * @property {Object} model.view
 * @property {ControlItemList} model.view.functions
 * @property {Object} request
 * @property {Object} request.param
 * @property {ControlItemList} request.param.encoding
 * @property {ControlItemList} request.param.location
 * @property {ControlItemList} targets
 * @property {Object} transport
 * @property {ControlItemList} transport.types
 * @property {Object} variable
 * @property {ControlItemList} variable.scope
 */

/**
 * @typedef {Object} PigField
 * @property {Object} allowed
 * @property {string|undefined} allowed.regex
 * @property {Array<string>|undefined} allowed.values
 * @property {Array<string>} attributes
 * @property {string} desc
 * @property {string} id
 * @property {string} name
 * @property {PigFieldType|undefined} subtype
 * @property {PigFieldType} type
 */

/**
 * @typedef {Object} PigFieldReference
 * @property {*} defaultValue
 * @property {string} fieldId
 * @property {string} id
 * @property {"model"|"query"|"result"} propertyOf
 */

/**
 * @typedef {PigFieldReference} PigFieldOperation
 * @property {boolean|undefined} _scalar - an internally managed property that we set if a request field's cardinality should be >1
 * @property {string} encoding
 * @property {"body"|"header"|"path"|"query"} location
 * @property {PigOperation} operation
 * @property {string} variable
 */

/**************** PostgreSQL Types ****************/
/**
 * @typedef {Object} PGSqlResult
 * @property {string} command
 * @property {Array<{dataTypeID:Number, format:string, name:string}>} fields
 * @property {Array<Object>} rows
 * @property {Number} rowCount
 */


/**************** Request Types ****************/
/**
 * A swagger request parameter
 * @typedef {Object} RequestParam
 * @property {*} originalValue
 * @property {*} value
 * @property {Array<string>} path
 * @property {SpecParameter} schema
 */


/**************** Specification Types ****************/
/**
 * Schema for a service's database
 * @typedef {Object} SpecDatabase
 * @property {string} connectionString
 * @property {Object|undefined} options
 * @property {string} name
 */

/**
 * Specification schema for a service's database collection
 * @typedef {Object} SpecDatabaseCollection
 * @property {string} id
 * @property {Array<SpecDatabaseIndex>} indices
 * @property {Object} model
 * @property {Array<SpecDatabaseField>} schema - it's a flattened field schema
 * @property {string} modelId
 * @property {string} name
 */

/**
 * Specification schema for a service's database field
 * @typedef {Object} SpecDatabaseField
 * @property {Array<string>|undefined} allowed
 * @property {Array<string>} attributes
 * @property {string} id
 * @property {string} description
 * @property {string} name
 * @property {PigFieldType} type
 * @property {PigFieldType} subType
 */

/**
 * Specification schema for a database index
 * @typedef {Object} SpecDatabaseIndex
 * @property {string} id
 * @property {string} name
 * @property {Object|undefined} options
 * @property {Array<string, value>} spec
 */

/**
 * Specification schema for a model
 * @typedef {Object} SpecModel
 * @property {string} id
 * @property {string} name
 * @property {Array<SpecDatabaseField>} fields
 */

/**
 * Specification schema for a path parameter
 * @typedef {Object} SpecParameter
 * @property {*} default
 * @property {string} description
 * @property {"body"|"header"|"path"|"query"} in
 * @property {string} name
 * @property {boolean} required
 * @property {Object|undefined} schema
 * @property {string|undefined} type
 * @property {SpecParameterExtension} "x-pig"
 */

/**
 * @typedef {Object} SpecParameterExtension
 * @property {Array<string>} attributes,
 * @property {string} dataPath,
 * @property {Array<{id:string, path:string}>} flattened,
 * @property {string} function,
 * @property {string} id,
 * @property {string} operation
 */

/**
 * Specification path schema
 * @typedef {Object} SpecPath
 * @property {Array<SpecParameter>} parameters
 * @property {Object<number, Object>} responses
 * @property {Array<string>} schemes
 * @property {string} summary
 * @property {Array<string>} tags
 * @property {SpecPathExtension} "x-pig"
 */

/**
 * A path's "x-pig" extension
 * @typedef {Object} SpecPathExtension
 * @property {string} modelId
 * @property {Array<SpecPathOperation>} operations
 * @property {{literals:Array<{id:string, name:string}>}} request
 * @property {{fields:Array<{id:string, name:string}>, flattened:Array<{id:string, path:string}>}} response
 * @property {string} viewId
 */

/**
 * Specification path operation
 * @typedef {Object} SpecPathOperation
 * @property {string} serviceId
 * @property {PigFunction} type
 */

/**
 * Specification schema for a service definition as found in our spec
 * @typedef {Object} SpecService
 * @property {string} class
 * @property {string} id
 * @property {string} name
 * @property {string} type
 * @property {Array<SpecDatabaseCollection>|undefined} collections
 * @property {SpecDatabase|undefined} database
 */
