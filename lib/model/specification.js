/**
 * User: curtis
 * Date: 2/12/18
 * Time: 11:15 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const deref=require("json-schema-deref-sync");
const {PigError}=require("pig-core").error;
const {Model}=require("./_base");
const constant=require("../common/constant");

/**
 * This guy dereferences all "$ref" in the specification definition.  Also allows us wiggle room for rendering own
 * view of an API and views of our own extensions
 * @typedef {Model} Specification
 */
class Specification extends Model {
	/**
	 * @param {Object} document - the whole enchilada. Extensions, if they exist should be in "x-pig"
	 */
	constructor(document) {
		super(deref(document));
		this._unadulterated=document;
	}

	/**
	 * Direct access to the spec that includes "$ref" dereferences. See <code>unadulterated</code> for non-inlined version
	 * @returns {Object}
	 */
	get document() {
		return this._data;
	}

	/**
	 * Returns our own extensions to the openAPI configuration if they have been set otherwise undefined
	 * @returns {Object|undefined}
	 */
	get extensions() {
		return this._data[constant.openapi.extensions.PIG];
	}

	/**
	 * This is a version of the spec that we have not dereferenced (inlined).  See <code>document</code> for inlined version
	 * @returns {Object}
	 */
	get unadulterated() {
		return this._unadulterated;
	}

	/**
	 * Accepts url style paths as well as dot delimited paths
	 * @param {string} path
	 * @param {*} dfault
	 * @returns {*}
	 */
	get(path, dfault=undefined) {
		path=path.replace(/\//g, ".");
		return _.get(this._data, path, dfault);
	}

	/**
	 * Returns object from the dictionary. Assumes all model objects will be found in <code>definitions</code>
	 * @param {string} path adopts uri convention.
	 * @returns {Object}
	 */
	getDefinition(path) {
		path=path.replace(/\//g, ".");
		if(!_.startsWith("definitions")) {
			path=`definitions.${path}`;
		}
		return _.get(this._data, path);
	}

	/**
	 * Finds model associated with id and returns it or throws error
	 * @param {string} id
	 * @returns {SpecModel}
	 * @throws {Error} if not found
	 */
	getModel(id) {
		const model=_.find(this._data[constant.openapi.extensions.PIG].model, {id});
		if(!model) {
			throw new PigError({
				details: `id=${id}`,
				instance: this,
				message: "could not find data model"
			});
		}
		return model;
	}
}

exports.Specification=Specification;
