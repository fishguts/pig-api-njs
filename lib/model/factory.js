/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const fs=require("fs-extra");
const path=require("path");
const jsyaml=require("js-yaml");
const {PigError}=require("pig-core").error;
const file=require("pig-core").file;
const http=require("pig-core").http;
const notification=require("pig-core").notification;
const {Application}=require("./application");
const {IncommingRequest}=require("./request");
const {Specification}=require("./specification");
const constant=require("../common/constant");

const storage={
	/**
	 * Manages our instances per application: application.local, application.dev, etc.
	 */
	application: {},
	gamuts: null,
	specification: null,
	nodenv: (process.env.NODE_ENV || constant.nodenv.DEVELOPMENT)
};

exports.application={
	/**
	 * Gets the configuration for the specified environment. Defaults to the current environment
	 * @param {string} nodenv
	 * @returns {Application}
	 */
	get(nodenv=storage.nodenv) {
		if(!storage.application[nodenv]) {
			const settings=file.relativePathToAbsolute({
				module,
				relativeToModuleRoot: "res/configuration/settings.yaml"
			});
			this.set(settings, nodenv);
		}
		return storage.application[nodenv];
	},

	/**
	 * Sets a configuration for the specified application.
	 * @param {Object} settings
	 * @param {string} nodenv
	 */
	set(settings, nodenv=storage.nodenv) {
		// we don't want mismatches with the environment name so if his differs from nodenv
		// then we correct it.
		if(_.get(settings, "environment.name")!==nodenv) {
			settings=Object.assign({}, settings);
			_.set(settings, "environment.name", nodenv);
		}
		storage.application[nodenv]=new Application(settings);
		this._applicationToEnvironment(storage.application[nodenv]);
		if(nodenv===storage.nodenv) {
			notification.emit(constant.event.SETTINGS_LOADED);
		}
	},

	/**
	 * Aligns the environment to our application
	 * @param {Application} application
	 * @private
	 */
	_applicationToEnvironment(application) {
		_.forEach(application.environment.variables, function(value, key) {
			process.env[key]=value;
		});
	}
};

exports.gamuts={
	/**
	 * @return {PigGamuts}
	 */
	get() {
		if(!storage.gamuts) {
			const path=file.relativePathToAbsolute({
				module,
				relativeToModuleRoot: "./res/configuration/model-gamuts.yaml"
			});
			storage.gamuts=file.readToJSONSync(path);
		}
		return storage.gamuts;
	}
};

exports.incommingRequest={
	/**
	 * Validates that all is okay and then creates an instance of IncommingRequest
	 * @param {Object} request
	 * @param {Specification} specification
	 * @returns {IncommingRequest}
	 * @throws {Error} if environment or request is not properly setup
	 */
	create(request, specification=exports.specification.get()) {
		if(!specification) {
			throw new PigError({
				details: "specification is not set",
				message: "library is not fully configured"
			});
		} else {
			const result=new IncommingRequest(request, specification);
			if(!result.extensions) {
				throw new PigError({
					message: "extension information is missing from the request",
					statusCode: http.status.code.BAD_REQUEST
				});
			}
			return result;
		}
	}
};

exports.specification={
	/**
	 * Gets instance that must be set by the hosting application
	 * @returns {Specification}
	 */
	get() {
		return storage.specification;
	},

	/**
	 * Creates an instance of Specification from <code>document</code> which should be the openAPI definition of the current project.
	 * We support two different formats for specs:
	 *  1. extensions (if there are any) are built into the openAPI document and are encoded within "x-pig"
	 *  2. extensions are in a standalone "extensions.yaml.mustache" file. We will do substitutions with our current nodenv.
	 * @param {Object|String} document if it is a string then it will be treated as a path
	 * @returns {Specification}
	 * @throws {Error} if anything goes wrong trying to load or create it
	 */
	set(document) {
		if(!_.isString(document)) {
			storage.specification=new Specification(document);
		} else {
			const stats=fs.lstatSync(document);
			if(!stats.isDirectory()) {
				storage.specification=new Specification(file.readToJSONSync(document));
			} else {
				const spec_path=path.join(document, "swagger.yaml"),
					ext_path=path.join(document, "extensions.yaml.mustache"),
					spec_data=file.readToJSONSync(spec_path);
				if(fs.pathExistsSync(ext_path)) {
					const ext_template=fs.readFileSync(ext_path, {encoding: "utf-8"}),
						ext_data=jsyaml.safeLoad(ext_template.replace(/{{nodenv}}/g, storage.nodenv));
					// console.log(JSON.stringify(ext_data, null, "\t"));
					storage.specification=new Specification(Object.assign({
						[constant.openapi.extensions.PIG]: ext_data
					}, spec_data));
				} else {
					storage.specification=new Specification(spec_data);
				}
			}
		}
		this._specificationToApplication(storage.specification);
		return storage.specification;
	},

	/**
	 * Means of un-setting the spec. It's not intended for production use but rather for testing.
	 * Good example of state data that makes testing a bit of a pain.
	 */
	reset() {
		storage.specification=null;
	},

	/**** Private Interface ****/
	/**
	 * Assembles a configuration that we may use to configure our application
	 * @param {Specification} specification
	 * @private
	 */
	_specificationToApplication(specification) {
		// Under normal circumstances there should be extensions. But let's be bullet proof. We will retain defaults for non-info properties.
		const application=exports.application.get();
		exports.application.set({
			environment: _.get(specification.extensions, "environment", application.raw.environment),
			info: specification.raw.info,
			log: _.get(specification.extensions, "log", application.raw.log)
		});
	}
};
