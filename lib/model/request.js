/**
 * User: curtis
 * Date: 2/17/18
 * Time: 9:04 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const http=require("pig-core").http;
const constant=require("../common/constant");
const {Model}=require("./_base");

/**
 * Wrapper around an incomming request.
 * @typedef {Model} IncommingRequest
 */
class IncommingRequest extends Model {
	/**
	 * @param {Object} request
	 * @param {Specification} specification
	 */
	constructor(request, specification) {
		super(request);
		this._specification=specification;
		this._stack=new DataStack(request);
	}

	/**
	 * Same as the "x-pig" extensions defined in the spec
	 * @returns {SpecPathExtension}
	 */
	get extensions() {
		return this._data.operation["x-pig"];
	}

	/**
	 * This is what comes to us via a swagger request. It is the operation details which is the object pointed to in the api spec by <code>path.[method]</code>
	 * @returns {*|{name: string, arguments: {requests: *[]}}|operation|{name, arguments}|{name: string, arguments: {requests: {insertOne: {document: {_id:
	 *     number, x: number}}}[], ordered: boolean, writeConcern: {w: number}}}}
	 */
	get operation() {
		return this._data.operation;
	}

	/**
	 * Gets the specification associated with this request
	 * @returns {Specification}
	 */
	get specification() {
		return this._specification;
	}

	/**
	 * Gets the data stack which are the current params and current body
	 * @returns {DataStack}
	 */
	get stack() {
		return this._stack;
	}

	/**
	 * Gets the schema for the body of the request
	 * @returns {SpecParameter|undefined}
	 */
	getBodySchema() {
		return _.get(this._data.params, "body.schema");
	}
	/**
	 * Gets the schema for the specified param
	 * @param {string} name
	 * @returns {SpecParameter}
	 */
	getParamSchema(name) {
		return _.get(this._data.params, `${name}.schema`);
	}
}

/**
 * This guy manages the values associated with the request. And allows operations to be applied to them
 * and pushed on it's stack.
 */
class DataStack {
	/**
	 * @param {Object} request
	 */
	constructor(request) {
		this._request=request;
		this._stack=[{
			params: _.reduce(request.params, function(result, value, key) {
				result[key]=value.value;
				return result;
			}, {}),
			result: undefined,
			contentType: undefined,
			statusCode: _(request.operation.responses)
				.keys()
				.map(Number)
				.filter((code)=>code<400)
				.sort()
				.value()[0] || http.status.code.OK
		}];
	}

	/**
	 * Finds the first param located in the body and returns its value.
	 * @returns {Object|undefined}
	 */
	get body() {
		const param=this._request.operation.parameters.find((param)=>param.in===constant.request.param.location.BODY);
		return param
			? this._stack[this._stack.length-1].params[param.name]
			: undefined;
	}

	/**
	 * Gets the values of the all request params.  Most likely this will be the original state, though this does live on the stack and supports updates.
	 * Note: everything is a param. Including the body.
	 * @returns {Object<string,*>}
	 */
	get params() {
		return this._stack[this._stack.length-1].params;
	}

	/**
	 * Gets the accumulated result.
	 * @returns {Object}
	 */
	get result() {
		return this._stack[this._stack.length-1].result;
	}

	/**
	 * Gets the result's content-type
	 * @returns {string}
	 */
	get contentType() {
		return this._stack[this._stack.length-1].contentType;
	}

	/**
	 * Gets the most recent contentType
	 * @returns {Number}
	 */
	get statusCode() {
		return this._stack[this._stack.length-1].statusCode;
	}

	/**
	 * Gets the last element. It will clone data <code>body</code> and <code>params</code> if you like but unless
	 * you are planning on updating incomming parameters (params or body) then this is unnecessary.
	 * @param {boolean} clone whether we deep clone incomming objects
	 * @returns {{params:Object<string,*>, result:Object, contentType:string, statusCode:Number}}
	 */
	get(clone=false) {
		// we always shallow clone it so that we don't inherit dumb mistakes
		const result=Object.assign({}, this._stack[this._stack.length-1]);
		if(clone) {
			result.params=_.cloneDeep(result.params);
		}
		return result;
	}

	/**
	 * Updates values that are included and defaults those that are not
	 * @param {Object} result
	 * @param {string} contentType - the result's content type
	 * @param {Object} params
	 * @param {Number} statusCode
	 * @returns {{contentType:String, params:Object<string,*>, result:Object, statusCode:Number}}
	 */
	push({result=undefined, contentType=undefined, params=undefined, statusCode=undefined}) {
		const last=this._stack[this._stack.length-1],
			updated={
				params: (params!==undefined) ? params : last.params,
				result: (result!==undefined) ? result : last.result,
				contentType: (contentType!==undefined) ? contentType : last.contentType,
				statusCode: (statusCode!==undefined) ? statusCode : last.statusCode
			};
		this._stack.push(updated);
		return updated;
	}
}

exports.IncommingRequest=IncommingRequest;
