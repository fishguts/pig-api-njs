/**
 * User: curtis
 * Date: 2/18/18
 * Time: 8:09 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");

class ServiceBase {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		this._configuration=configuration;
		this._subServices=[];
	}

	/**
	 * Gets the configuration that this service was built upon
	 * @returns {SpecService}
	 */
	get configuration() { return this._configuration; }
	/**
	 * Gets the id of this service
	 * @returns {string}
	 */
	get id() { return this._configuration.id; }
	/**
	 * Gets the name of this service
	 * @returns {string}
	 */
	get name() { return this._configuration.name; }
	/**
	 * Gets the class of this service
	 * @returns {string}
	 */
	get class() { return this._configuration.class; }
	/**
	 * Gets the type of this service
	 * @returns {string}
	 */
	get type() { return this._configuration.type; }

	/**
	 * A service such as a database has components such as tables and collections that
	 * may be referenced directly.  It is this service's job to serve up children if they exist
	 * @returns {[ServiceBase]}
	 */
	get subServices() { return this._subServices; }
	/**
	 * @param {[ServiceBase]} subServices
	 */
	set subServices(subServices) {
		this._subServices=subServices;
	}

	/**
	 * Gets property from this service's configuration. Is safe and supports a default which is the attraction.
	 * @param {string} path
	 * @param {*} dfault
	 * @returns {*}
	 */
	getProperty(path, dfault=undefined) {
		return _.get(this._configuration, path, dfault);
	}

	/**
	 * Initialize this class. Does nothing at this level.
	 * @returns {Promise}
	 */
	async initialize() {
		return Promise.resolve();
	}

	/**
	 * Allows this service to clean up allocated resources
	 * @returns {Promise<void>}
	 */
	async shutdown() {
		return Promise.all(this._subServices.map(service=>service.shutdown()));
	}
}

exports.ServiceBase=ServiceBase;

