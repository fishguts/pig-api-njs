/**
 * User: curtis
 * Date: 2/18/18
 * Time: 8:19 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {PigError}=require("pig-core").error;
const {HttpService}=require("./http");
const {TransformService}=require("./transform");
const database_factory=require("./database/_factory");
const constant=require("../common/constant");

/**
 * Identifies the service configuration and creates a ServiceBase
 * @param {SpecService} configuration
 * @returns {ServiceBase}
 */
exports.createService=function(configuration) {
	switch(configuration.type) {
		case constant.api.service.type.MONGO: {
			return database_factory.createMongo(configuration);
		}
		case constant.api.service.type.PGSQL: {
			return database_factory.createPGSql(configuration);
		}
		case constant.api.service.type.HTTP: {
			return new HttpService(configuration);
		}
		case constant.api.service.type.TRANSFORM: {
			return new TransformService(configuration);
		}
		case constant.api.service.type.TEST_DB: {
			return database_factory.createTest(configuration);
		}
		default: {
			throw new PigError({
				instance: this,
				details: `unknown service class '${configuration.class}'`,
				message: `could not create service '${configuration.name}'`
			});
		}
	}
};
