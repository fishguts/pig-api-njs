/**
 * User: curtis
 * Date: 2/18/18
 * Time: 8:32 PM
 * Copyright @2018 by Xraymen Inc.
 */
const _=require("lodash");
const {PigError}=require("pig-core").error;
const log=require("pig-core").log;
const service_factory=require("./factory");

/**
 * Loads services from a specification and then acts as a service
 */
class Services {
	constructor() {
		/**
		 * @type {Specification}
		 * @private
		 */
		this._specification=null;
		this._services={};
	}

	/**
	 * Gets service by his id
	 * @param {string} id
	 * @returns {ServiceBase}
	 */
	getById(id) {
		const service=this._services[id];
		if(service) {
			return service;
		} else {
			throw new PigError({
				instance: this,
				message: `unknown service '${id}'`
			});
		}
	}

	/**
	 * Initialize our services layer from the specification
	 * @param {Specification} specification
	 * @returns {Promise}
	 */
	initialize(specification) {
		if(this._specification===specification) {
			return Promise.resolve();
		} else {
			this._services={};
			this._specification=specification;
			this._configureLog(specification);
			const promises=_.get(specification.extensions, "services", [])
				.reduce((result, configuration)=> {
					try {
						const service=service_factory.createService(configuration);
						this._services[service.id]=service;
						Object.assign(this._services, _.keyBy(service.subServices, "id"));
						// we want to always make sure we initialize the sub-services before the parent as we assume there are dependencies.
						// But we don't care about the order of the parent level services.  Hence the following:
						result.push(service.initialize()
							.then(()=>Promise.all(service.subServices.map((_service)=>_service.initialize())))
						);
					} catch(error) {
						result.push(Promise.reject(error));
					}
					return result;
				}, []);
			return Promise.all(promises);
		}
	}

	/**
	 * Calls shutdown on each service and completely resets <code>this</code> instance
	 * @returns {Promise}
	 */
	shutdown() {
		const services=_.values(this._services);
		this._specification=null;
		this._services={};
		return Promise.all(services.map(service=>service.shutdown()));
	}

	/********************* Private Interface *********************/
	/**
	 * Sets up the log as per the spec's log configuration
	 * @param {Specification} specification
	 * @private
	 */
	_configureLog(specification) {
		const document=specification.document;
		log.configure({
			applicationName: document.info.title,
			configuration: document["x-pig"].log,
			environment: document["x-pig"].environment.name
		});
	}
}

module.exports=new Services();
