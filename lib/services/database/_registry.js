/**
 * User: curtis
 * Date: 8/30/18
 * Time: 10:23 PM
 * Copyright @2018 by Xraymen Inc.
 */

const pig_core=require("pig-core");

/**
 * Works with a client to:
 * - setup a database according to an uncreated db
 * - make modifications with updates to an already existing db.
 * - save info in the service's databases
 * It's only partially implemented. We are largely cheating and using IDs vs. names.
 * The goal: https://bitbucket.org/fishguts/pig-api-njs/issues/6/better-handling-of-updates-to-an-existing
 */
class DatabaseRegistry {
	/**************** Types ****************/
	/**
	 * Function through which we will ask for "pig-internals" which is information that we want to load and save.
	 * It will all be JSON and will be keyed by id.
	 * @typedef {Function} loadDataCallback
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @returns {Promise<Object>}
	 */

	/**
	 * Function through which we will our caller to save data.
	 * @typedef {Function} saveDataCallback
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @param {Object} data
	 * @returns {Promise<void>}
	 */

	/**
	 * Defines a refactor table callback function
	 * todo: this is missing modification of constraints
	 * @typedef {Function} refactorTableCallback
	 * @param {string} collectionId
	 * @param {string} collectionName
	 * @param {Array<SpecDatabaseField>} addFields
	 * @param {Array<SpecDatabaseField>} removeFields
	 * @param {Array<SpecDatabaseField>} renameFields
	 * @param {Array<SpecDatabaseField>} retypeFields
	 * @returns {Promise<void>}
	 */

	/**
	 * Constructor
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		this._newConfiguration=configuration;
		/**
		 * @type {SpecService}
		 */
		this._oldConfiguration=null;
		/**
		 * @type {loadDataCallback}
		 */
		this._load=null;
		/**
		 * @type {saveDataCallback}
		 */
		this._save=null;
	}

	/**************** Public Interface ****************/
	/**
	 * Initializes this guy.
	 * @param {loadDataCallback} load
	 * @param {saveDataCallback} save
	 * @return {Promise<void>}
	 */
	async initialize({load, save}) {
		this._load=load;
		this._save=save;
		return this._load("urn:pig:db", "urn:pig:model", this._newConfiguration.id)
			.then(data=>{
				this._oldModel=data;
			})
			.catch(error=>{
				pig_core.log.warn(`${this.constructor.name}.intialize(): load failed - ${error.message}? If the data does not exist that is fine but you will want to catch the error.`);
			});
	}

	/**
	 * Should call upon completion
	 * @return {Promise<void>}
	 */
	async complete() {
		return this._save("urn:pig:db", "urn:pig:model", this._newConfiguration.id, this._newConfiguration);
	}

	/**
	 * Figures out what needs to be done to get setup and ultimately connected to database
	 * @param {function(id:string, name:string, options:Object):Promise<void>} connect
	 * @param {function(id:string, name:string, options:Object):Promise<void>} create
	 * @param {function(nameFrom:string, nameTo:string):Promise<void>} rename
	 * @returns {Promise}
	 */
	async setupDatabase({connect, create, rename}) {
		// see class documentation for first pass goals if this does not make sense
		return connect(this._newConfiguration.id, this._newConfiguration.id, this._newConfiguration.database.options)
			.catch(error=>{
				return create(this._newConfiguration.id, this._newConfiguration.id, this._newConfiguration.database.options)
					.catch(error=>{
						throw new pig_core.error.PigError({
							instance: this,
							error: error,
							message: `failed to setup ${this._newConfiguration.type} database`
						});
					});
			});
	}

	/**
	 * Figures out what needs to be done to get setup and ultimately connected to all collections in DB
	 * @param {function(id:string, name:string, options:(Object|undefined)):Promise<void>} connect
	 * @param {function(id:string, name:string, options:(Object|undefined), Array<SpecDatabaseField>):Promise<void>} create
	 * @param {refactorTableCallback} refactor
	 * @param {function(nameFrom:string, nameTo:string):Promise<void>} rename
	 * @returns {Promise}
	 */
	async setupCollections({connect, create, refactor, rename}) {
		// see class documentation for first pass goals if this does not make sense
		return Promise.all(this._newConfiguration.collections.map(collection=>{
			return connect(collection.id, collection.id, collection.options)
				.catch(error=>{
					return create(collection.id, collection.id, collection.options, collection.schema)
						.catch(error=>{
							throw new pig_core.error.PigError({
								instance: this,
								error: error,
								message: `failed to setup ${this._newConfiguration.type} collection`
							});
						});
				});
		}));
	}

	/**
	 * Figures out what needs to be done to get setup and ultimately connected to database
	 * todo: this guy needs to be overhauled. Based on what we find in the registry:
	 *  - add new indexes
	 *  - remove indexes no longer referenced
	 *  - remove and create indexes that have been changed: name (imply field change), fields, order, ect..
	 * @param {function(collectionId:string, collectionName:string, index:SpecDatabaseIndex):Promise<void>} create
	 * @param {function(collectionId:string, collectionName:string, index:SpecDatabaseIndex):Promise<void>} remove
	 * @returns {Promise}
	 */
	async setupIndices({create, remove}) {
		// see class documentation for first pass goals if this does not make sense
		return Promise.all(this._newConfiguration.collections.map(collection=>{
			return Promise.all(collection.indices.map(create.bind(null, collection.id, collection.id)));
		}));
	}
}

exports.DatabaseRegistry=DatabaseRegistry;
