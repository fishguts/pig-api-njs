/**
 * User: curtis
 * Date: 7/22/18
 * Time: 12:50 AM
 * Copyright @2018 by Xraymen Inc.
 */

const {DatabaseCollection}=require("../collection");

/**
 * Interface for testing collection logic
 * @typedef {DatabaseCollection} TestCollection
 */
class TestCollection extends DatabaseCollection {
	/**
	 * @param {SpecDatabaseCollection} configuration
	 * @param {TestConnection} connection
	 */
	constructor(configuration, connection) {
		super(configuration);
		this.connection=connection;
	}

	/**
	 * Sets this collection up for action
	 * @returns {Promise}
	 */
	initialize() {
		return Promise.resolve();
	}

	/**** Interface definition ****/
	/**
	 * Counts the number of documents matching the query
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseCountResult>}
	 */
	count(filters) {
		return Promise.resolve(1);
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {Promise<DatabaseCreateManyResult>}
	 */
	createMany({
		attributes,
		data
	}) {
		return Promise.resolve([
			{_id: "dummy"}
		]);
	}

	/**
	 * Deletes 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteManyResult>}
	 */
	deleteMany({
		attributes,
		filters
	}) {
		return Promise.resolve({count: 1});
	}

	/**
	 * Gets distinct values for the response field specified in attributes after filtered by filter if there is one.
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDistinctResult>}
	 */
	distinct({
		attributes,
		filters
	}) {
		return Promise.resolve(["dummy"]);
	}

	/**
	 * Find 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseFindManyResult>}
	 */
	findMany({
		attributes,
		filters
	}) {
		return Promise.resolve([
			{_id: "dummy"}
		]);
	}

	/**
	 * Updates a single document identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the document that should be updated
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @returns {Promise<DatabaseUpdateOneResult>}
	 */
	updateOne({
		attributes,
		data,
		filters,
		upsert=false,
		retrieve=true
	}) {
		return (retrieve)
			? Promise.resolve({_id: "dummy"})
			: Promise.resolve({count: 1});
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the documents that should be updated
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @returns {Promise<DatabaseUpdateManyResult>}
	 */
	updateMany({
		attributes,
		data,
		filters,
		retrieve=true,
		upsert=false
	}) {
		return (upsert)
			? Promise.resolve([
				{_id: "dummy"}
			])
			: Promise.resolve({
				count: 1
			});
	}
}

exports.TestCollection=TestCollection;
