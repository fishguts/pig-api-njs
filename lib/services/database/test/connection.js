/**
 * User: curtis
 * Date: 7/22/18
 * Time: 12:50 AM
 * Copyright @2018 by Xraymen Inc.
 */


const {ServiceBase}=require("../../_base");

/**
 * A connection we use for testing
 * @typedef {ServiceBase} TestConnection
 */
class TestConnection extends ServiceBase {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		super(configuration);
	}

	/**** Implementation of ServiceBase functionality ****/
	/**
	 * Sets up this database for action
	 * @returns {Promise}
	 */
	async initialize() {
		return Promise.resolve();
	}
}

exports.TestConnection=TestConnection;
