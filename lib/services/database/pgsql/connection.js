/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {PGSqlSetup}=require("./_setup");
const {ServiceBase}=require("../../_base");

/**
 * Master and commander of a postgreSQL connection.
 * @typedef {ServiceBase} PGSqlConnection
 */
class PGSqlConnection extends ServiceBase {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		super(configuration);
		/**
		 * @type {Pool}
		 */
		this._pool=null;
	}

	/**
	 * Gets our instance of a postgres connection pool
	 * @returns {Pool}
	 */
	get pool() {
		assert.toLog(this._pool, "accessing pool before it is set?");
		return this._pool;
	}

	/**** Implementation of ServiceBase functionality ****/
	/**
	 * Sets up this postgreSQL database for action
	 * @returns {Promise}
	 */
	async initialize() {
		const setup=new PGSqlSetup(this.configuration);
		return setup.execute()
			.then(()=>{
				this._pool=setup.pool;
			});
	}

	/**
	 * Relinquishes whatever connections we have.
	 * @returns {Promise<void>}
	 */
	async shutdown() {
		if(!this._pool) {
			return Promise.resolve();
		} else {
			const pool=this._pool;
			this._pool=null;
			return pool.end();
		}
	}
}

exports.PGSqlConnection=PGSqlConnection;
