/**
 * User: curtis
 * Date: 9/1/18
 * Time: 6:03 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const http=require("pig-core").http;
const pig_core=require("pig-core");
const _sql=require("../_sql");
const {Client, Pool}=require("./_pg");
const {DatabaseRegistry}=require("../_registry");


/**
 * Sets up postgres databases.
 */
class PGSqlSetup {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		this._configuration=configuration;
		/**
		 * Pool of connections to model's database.
		 * @type {Pool}
		 */
		this.pool=null;
		/**
		 * Map of database name's to client connections
		 * @type {Client}
		 */
		this._clientMap={};
	}

	/**
	 * Sets up our service's postgreSQL database which includes:
	 * - making sure this service's model exists in the form of databases and tables and indices
	 * - leaves connections up to client. Would be a mistake to persist it....as we do with mongo.
	 * @returns {Promise<void>}
	 */
	async execute() {
		const registry=new DatabaseRegistry(this._configuration),
			_onComplete=result=>{
				const _onExit=()=>{
					this.reset();
					if(result instanceof Error) {
						PGSqlSetup._disconnectClient(this.pool); this.pool=null;
						return Promise.reject(result);
					} else {
						return Promise.resolve();
					}
				};
				return registry.complete()
					.then(_onExit, _onExit);
			};
		return this._createConnection()
			.then(registry.initialize.bind(registry, {
				load: this._loadRegistryData.bind(this),
				save: this._saveRegistryData.bind(this)
			}))
			.then(()=>registry.setupDatabase({
				connect: this._connectDatabase.bind(this),
				create: this._createDatabase.bind(this),
				rename: this._renameDatabase.bind(this)
			}))
			.then(()=>registry.setupCollections({
				connect: this._connectTable.bind(this),
				create: this._createTable.bind(this),
				refactor: this._refactorTable.bind(this),
				rename: this._renameTable.bind(this)
			}))
			.then(()=>registry.setupIndices({
				create: this._createIndex.bind(this),
				remove: this._removeIndex.bind(this)
			}))
			.then(_onComplete, _onComplete);
	}

	/**
	 * Disconnects from all connections setup during execute
	 */
	reset() {
		Object.values(this._clientMap)
			.filter(client=>client!==this.pool)
			.forEach(PGSqlSetup._disconnectClient);
		this._clientMap={};
	}


	/**************************** Utility Interface ****************************/
	/**
	 * Connects to the pg server.
	 * @param {string} databaseName
	 * @param {class} ClientType
	 * @returns {Promise<Client|Pool>}
	 * @private
	 */
	_createConnection(databaseName="postgres", ClientType=Client) {
		if(this._clientMap.hasOwnProperty(databaseName)) {
			return Promise.resolve(this._clientMap[databaseName]);
		} else {
			const connectString=`${this._configuration.database.connectionString}/${databaseName}`,
				client=new ClientType({
					connectionString: connectString
				});
			return client.connect()
				.then((poolClient)=> {
					pig_core.log.verbose(`PGSqlSetup._createConnection(): connection to ${connectString} succeeded`);
					this._clientMap[databaseName]=client;
					// if we are connecting a pool then we will get a client back to play with. We don't want him
					// quite yet but we need need to put him back in the pool otherwise our pool will never close.
					if(poolClient) {
						poolClient.release();
					}
					return client;
				})
				.catch((error)=> {
					throw new pig_core.error.PigError({
						details: `connectionString=${connectString}`,
						error: error,
						instance: this,
						message: "failed to connect to postgreSQL server",
						statusCode: error.code==="3D000" ? http.status.code.NOT_FOUND : 500
					});
				});
		}
	}

	/**
	 * @param {Client|Pool} client
	 */
	static _disconnectClient(client) {
		if(client) {
			client.end()
				.catch(error=>{
					pig_core.log.warn(`PGSqlSetup._disconnectClient() failed: ${error}`);
				});
		}
	}

	/**
	 * Makes sure it exists and creates it if it does not
	 * @param {string} name
	 * @returns {Promise}
	 * @private
	 */
	_ensureDatabase(name) {
		return this._createConnection("postgres")
			.then(client=>client.query(`SELECT 1 FROM pg_database WHERE datname='${name}'`)
				.then(result=>{
					if(result.rows.length>0) {
						return Promise.resolve();
					} else {
						return client.query(`CREATE DATABASE "${name}"`);
					}
				})
			);
	}

	/**************** Registry callback support ****************/
	/**
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @return {Promise<Object>}
	 * @private
	 */
	_loadRegistryData(databaseName, collectionId, dataId) {
		return this._ensureDatabase(databaseName)
			.then(()=>this._createConnection(databaseName))
			.then(client=>client.query(`SELECT data FROM "${collectionId}" WHERE id='${dataId}'`))
			.then(result=>_.get(result.rows, "0.data", null))
			.catch(error=>{
				// "42P01" = table is undefined
				return (error.code==="42P01")
					? Promise.resolve(null)
					: Promise.reject(error);
			});
	}

	/**
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @param {Object} data
	 * @return {Promise<void>}
	 * @private
	 */
	_saveRegistryData(databaseName, collectionId, dataId, data) {
		return this._ensureDatabase(databaseName)
			.then(()=>this._createConnection(databaseName))
			.then(client=>
				client.query(`CREATE TABLE IF NOT EXISTS "${collectionId}" (id TEXT PRIMARY KEY, data JSON)`)
					.then(()=> {
						return client.debugQuery(`INSERT INTO "${collectionId}" (id, data) VALUES ($1, $2) ON CONFLICT (id) DO UPDATE SET data=$2`, [dataId, data]);
					})
			);
	}

	/**
	 * @param {string} id
	 * @param {string} name
	 * @param {Object} options
	 * @returns {Promise<void>}
	 */
	_connectDatabase(id, name, options) {
		return this._createConnection(name, Pool)
			.then(pool=>this.pool=pool);
	}

	/**
	 * @param {string} id
	 * @param {string} name
	 * @param {Object} options
	 * @returns {Promise<void>}
	 */
	_createDatabase(id, name, options) {
		return this._clientMap["postgres"].query(`CREATE DATABASE "${name}"`)
			.then(this._connectDatabase.bind(this, id, name));
	}

	/**
	 * @param {string} nameFrom
	 * @param {string} nameTo
	 * @returns {Promise<void>}
	 */
	_renameDatabase(nameFrom, nameTo) {
		return this._clientMap["postgres"].query(`ALTER DATABASE "${nameFrom}" RENAME TO "${nameTo}"`);
	}

	/**************** Tables ****************/
	/**
	 * @param {string} id
	 * @param {string} name
	 * @param {Object} options
	 * @returns {Promise<void>}
	 */
	_connectTable(id, name, options) {
		// we don't persist any sort of connection. Our job here is to figure out whether it exists
		return this.pool.query(`SELECT to_regclass('public."${name}"')`)
			.then(result=>{
				return (result.rows[0].to_regclass)
					? Promise.resolve()
					: Promise.reject(new pig_core.error.PigError({statusCode: http.status.code.NOT_FOUND}));
			});
	}

	/**
	 * @param {string} id
	 * @param {string} name
	 * @param {Object} options
	 * @param {Array<SpecDatabaseField>} fields
	 * @returns {Promise<void>}
	 */
	_createTable(id, name, options, fields) {
		const fieldsText=fields
			.reduce((result, field)=>{
				const type=_sql.fragment.fieldToColumnType(field);
				if(type) {
					result.push(`"${field.name}" ${type} ${_sql.fragment.fieldToColumnAttributes(field)}`);
				}
				return result;
			}, [])
			.join(",");
		return this.pool.query(`CREATE TABLE "${name}" (${fieldsText})`);
	}

	/**
	 * defines a refactor table callback function
	 * @callback refactorTableCallback
	 * @param {string} collectionId
	 * @param {string} collectionName
	 * @param {Array<SpecDatabaseField>} addFields
	 * @param {Array<SpecDatabaseField>} removeFields
	 * @param {Array<SpecDatabaseField>} renameFields
	 * @param {Array<SpecDatabaseField>} retypeFields
	 * @returns {Promise<void>}
	 */
	_refactorTable(collectionId, collectionName, addFields, removeFields, renameFields, retypeFields) {
		const actions=[]
			.concat(addFields.map(field=>`ADD COLUMN "${field.name}" ${_sql.fragment.fieldToColumnType(field)} ${_sql.fragment.fieldToColumnAttributes(field)}`))
			.concat(removeFields.map(field=>`DROP COLUMN "${field.name}"`))
			.concat(renameFields.map(field=>`RENAME COLUMN "${field.from}" TO ${field.name}`))
			.concat(retypeFields.map(field=>`ALTER COLUMN "${field.name}" SET DATA TYPE "${_sql.fragment.fieldToColumnType(field)}"`));
		return this.pool.queryDebug(`ALTER TABLE "${collectionName}" ${actions.join(", ")}`);
	}

	/**
	 * @param {string} nameFrom
	 * @param {string} nameTo
	 * @returns {Promise<void>}
	 */
	_renameTable(nameFrom, nameTo) {
		return this.pool.query(`ALTER TABLE "${nameFrom}" RENAME TO "${nameTo}"`);
	}

	/**************** Indices ****************/
	/**
	 * @param {string} collectionId
	 * @param {string} collectionName
	 * @param {SpecDatabaseIndex} index
	 * @returns {Promise<void>}
	 */
	_createIndex(collectionId, collectionName, index) {
		const indexText=_(index.spec)
			.map(fieldMap=>{
				const name=Object.keys(fieldMap)[0],
					value=Object.values(fieldMap)[0];
				return (value>0)
					? `"${name}" ASC`
					: `"${name}" DESC`;
			})
			.join(",");
		return this.pool.query(`CREATE INDEX IF NOT EXISTS "${index.name}" ON "${collectionName}" (${indexText})`);
	}

	/**
	 * @param {string} collectionId
	 * @param {string} collectionName
	 * @param {SpecDatabaseIndex} index
	 * @returns {Promise<void>}
	 */
	_removeIndex(collectionId, collectionName, index) {
		return this.pool.query(`DROP INDEX "${index.name}"`);
	}
}

exports.PGSqlSetup=PGSqlSetup;
