/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {StatementFactory}=require("../_sql/statement");
const {DatabaseCollection}=require("../collection");

/**
 * Interface to a PostgreSQL table.
 * @typedef {DatabaseCollection} PGSqlCollection
 */
class PGSqlCollection extends DatabaseCollection {
	/**
	 * @param {SpecDatabaseCollection} configuration
	 * @param {PGSqlConnection} connection
	 */
	constructor(configuration, connection) {
		super(configuration);
		this._connection=connection;
		// todo: [#6] - refactor table name to be the "name"
		this._table=configuration.id;
	}

	/**** Interface definition ****/
	/**
	 * Counts the number of documents matching the query
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseCountResult>}
	 */
	count(filters) {
		const factory=new StatementFactory(this._table),
			{params, text}=factory.count(filters);
		return this._connection.pool.debugQuery(text, params)
			.then(result=>result.rowCount);
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {Promise<DatabaseCreateManyResult>}
	 */
	createMany({
		attributes,
		data
	}) {
		const factory=new StatementFactory(this._table),
			{params, text}=factory.createMany({attributes, data});
		return this._connection.pool.debugQuery(text, params)
			.then(this._resultToResponse.bind(this, null));
	}

	/**
	 * Deletes 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteManyResult>}
	 */
	deleteMany({
		attributes,
		filters
	}) {
		const factory=new StatementFactory(this._table),
			{params, text}=factory.deleteMany({attributes, filters});
		return this._connection.pool.debugQuery(text, params)
			.then(result=>({deleted: result.rowCount}));
	}

	/**
	 * Gets distinct values for the response field specified in attributes after filtered by filter if there is one.
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDistinctResult>}
	 */
	distinct({
		attributes,
		filters
	}) {
		const columnName=attributes.fields.value[0],
			factory=new StatementFactory(this._table),
			{params, text}=factory.distinct({attributes, filters});
		return this._connection.pool.debugQuery(text, params)
			.then(result=>_.map(result.rows, columnName));
	}

	/**
	 * Find 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseFindManyResult>}
	 */
	findMany({
		attributes,
		filters
	}) {
		const factory=new StatementFactory(this._table),
			{params, text}=factory.findMany({attributes, filters});
		return this._connection.pool.debugQuery(text, params)
			.then(this._resultToResponse.bind(this, []));
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the documents that should be updated
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @returns {Promise<DatabaseUpdateManyResult>}
	 */
	updateMany({
		attributes,
		data,
		filters,
		retrieve=true,
		upsert=false
	}) {
		const factory=new StatementFactory(this._table),
			{params, text}=factory.updateMany({attributes, data, filters, retrieve, upsert});
		return this._connection.pool.debugQuery(text, params)
			.then(result=>(retrieve)
				? this._resultToResponse(result, [])
				: {updated: result.rows.length}
			);
	}

	/**************** Private Interface ****************/
	/**
	 * Translates a query into an array of objects
	 * @param {*} dfault - default value to return if there is no response. In most cases we will return an
	 * 	empty array. But our create spec is to return <code>null</code> if the user does not want any results.
	 * @param {PGSqlResult} result
	 * @return {Array<Object>|[]}
	 * @private
	 */
	_resultToResponse(dfault, result) {
		if(result.rows.length===0) {
			return dfault;
		} else {
			const pgFieldMap=_.keyBy(result.fields, "name");
			return result.rows.map(row=>{
				return _.reduce(row, (data, value, property)=>{
					// parsing in <code>pg-types.lib.textParsers.js</code> for bigint simply casts to a String.
					// I think this is a bug? Either way we care 'cause we use them by default. So we convert them.
					const pgField=pgFieldMap[property];
					if(pgField.dataTypeID===20) {
						value=Number(value);
					}
					return _.set(data, property, value);
				}, {});
			});
		}
	}
}

exports.PGSqlCollection=PGSqlCollection;
