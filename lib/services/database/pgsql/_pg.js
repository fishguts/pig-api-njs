/**
 * User: curtis
 * Date: 9/4/18
 * Time: 8:51 PM
 * Copyright @2018 by Xraymen Inc.
 */

const pg=require("pg");

/* eslint-disable no-console */

/**
 * Extends the postgreSQL driver's client.
 * @typedef {pg.Client} PgClient
 */
class PgClient extends pg.Client {
	/**
	 * Executes query with verbose logging
	 * @param {string} query
	 * @param {Array<string>|undefined}values
	 * @returns {Promise<{command:string, rows:Array<Object>}>}
	 */
	debugQuery(query, values) {
		return _debugQuery(this, query, values);
	}
}

/**
 * Extends the postgreSQL driver's client.
 * Note: we are not really extending it because the core lib uses a constructor function that instantiates his own
 * instance of Pool.  But we can still extend that instance.  See our constructor.
 * @typedef {pg.Pool} PgPool
 */
class PgPool extends pg.Pool {
	/**
	 * Important: see class comment
	 * @param {Object} config
	 */
	constructor(config) {
		const instance=super(config);
		instance.debugQuery=_debugQuery.bind(null, instance);
	}

	/**
	 * Executes query with verbose logging
	 * @param {string} query
	 * @param {Array<string>|undefined}values
	 * eslint-disable-next-line @returns {Promise<{command:string, rows:Array<Object>}>}
	 */
	debugQuery(query, values) {
		// see class documentation regarding why there is no functionality here
	}
}

exports.Client=PgClient;
exports.Pool=PgPool;


/**************** Private Interface ****************/
/**
 * Executes query with verbose logging
 * @param {Client} client
 * @param {string} query
 * @param {Array<string>|undefined}values
 * @returns {Promise<{command:string, rows:Array<Object>}>}
 * @private
 */
function _debugQuery(client, query, values) {
	return client.query(query, values)
		.then(result=>{
			console.log(`pg.${client.constructor.name}.query("${query}", ${JSON.stringify(values)})`);
			console.log(`results=${JSON.stringify(result, null, "\t")}`);
			return result;
		})
		.catch(error=>{
			console.error(`pg.${client.constructor.name}.query("${query}", ${JSON.stringify(values)})`);
			console.error(`error=${JSON.stringify(Object.assign({message: error.message}, error), null, "\t")}`);
			throw error;
		});
}

