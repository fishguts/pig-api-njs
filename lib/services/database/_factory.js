/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");

/**
 * Creates and configures a mongo connection as well as all of the collections within
 * @param {SpecService} configuration
 * @returns {ServiceBase}
 * @private
 */
exports.createMongo=function(configuration) {
	const {MongoConnection}=require("./mongo/connection");
	const {MongoCollection}=require("./mongo/collection");
	const connection=new MongoConnection(configuration);
	connection.subServices=_.map(configuration.collections, (collection)=>{
		// note: class and type are required properties of a <code>Service</code>
		return new MongoCollection(Object.assign({
			class: configuration.class,
			type: configuration.type
		}, collection), connection);
	});
	return connection;
};

/**
 * Creates and configures a mongo connection as well as all of the collections within
 * @param {SpecService} configuration
 * @returns {ServiceBase}
 * @private
 */
exports.createPGSql=function(configuration) {
	const {PGSqlConnection}=require("./pgsql/connection");
	const {PGSqlCollection}=require("./pgsql/collection");
	const connection=new PGSqlConnection(configuration);
	connection.subServices=_.map(configuration.collections, (collection)=>{
		// note: class and type are required properties of a <code>Service</code>
		return new PGSqlCollection(Object.assign({
			class: configuration.class,
			type: configuration.type
		}, collection), connection);
	});
	return connection;
};

/**
 * Sets up a test configuration.  All are created as connections.
 * @param {SpecService} configuration
 * @returns {ServiceBase}
 * @private
 */
exports.createTest=function(configuration) {
	const {TestConnection}=require("./test/connection");
	const {TestCollection}=require("./test/collection");
	const connection=new TestConnection(configuration);
	connection.subServices=_.map(configuration.collections, (collection)=>{
		// note: class and type are required properties of a <code>Service</code>
		return new TestCollection(Object.assign({
			class: configuration.class,
			type: configuration.type
		}, collection), connection);
	});
	return connection;
};
