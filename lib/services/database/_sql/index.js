/**
 * User: curtis
 * Date: 9/3/18
 * Time: 11:26 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const map=require("./map");
const constant=require("../../../common/constant");

/**
 * Creates fragments of SQL statement
 */
const fragment={
	/**
	 * Translates the specified field to postgresql attributes
	 * @param {SpecDatabaseField} field
	 * @returns {string}
	 */
	fieldToColumnAttributes(field) {
		const attributes=[];
		if(field.defaultValue!==undefined) {
			attributes.push(`DEFAULT ${field.defaultValue}`);
		}
		return _(field.attributes)
			.reduce((result, attribute)=> {
				if(attribute===constant.field.attribute.AUTO_ID) {
					// note: we handle auto-incrementing in <code>this.fieldToColumnType</code>
					result.push("PRIMARY KEY");
				} else if(attribute===constant.field.attribute.REQUIRED) {
					result.push("NOT NULL");
				}
				return result;
			}, attributes)
			.join(" ");
	},

	/**
	 * Translates the specified field to a postgreSQL type
	 * @param {SpecDatabaseField} field
	 * @returns {string|null}
	 */
	fieldToColumnType(field) {
		let pgType=map.type.fromPig[field.type];
		if(pgType) {
			// there are some exceptions. Let's see if he needs "upgrading"
			if(_.includes(field.attributes, constant.field.attribute.AUTO_ID)) {
				if((field.type===constant.field.type.ID)
					|| (field.type===constant.field.type.INTEGER)
					|| (field.type===constant.field.type.NUMBER)) {
					return "BIGSERIAL";
				} else {
					assert.toLog(false, `auto-id with type ${field.type} not handled`);
				}
			}
			return pgType;
		}
		// there are some types that need more love
		if(field.type===constant.field.type.ARRAY) {
			pgType=map.type.fromPig[field.subType];
			if(pgType) {
				return `${pgType}[]`;
			}
		}
		assert.toLog(field, `unknown field.type - field=${JSON.stringify(field)}`);
		return null;
	}
};

/**
 * Conversion to and from the DB space. Drivers will probably handle all of this at least for parameterized queries
 */
const value={
	/**
	 * SQL's reliance on single quotes are a pain when single quotes are in your data. So they must be escaped or
	 * thanks to "dollar quoted string constants", you may redefine quotes.
	 * Note: not all RDBMSs support dollar quoting:
	 *   postgres: https://www.postgresql.org/docs/9.1/static/sql-syntax-lexical.html#SQL-SYNTAX-DOLLAR-QUOTING
	 * @param {*} value - will convert using toString before escaping so safe for simple types
	 * @param {boolean} dollar - whether to use dollar method or not. Makes most sense for long strings.
	 * @param {string} quote
	 * @returns {string}
	 */
	escape(value, {
		dollar=false,
		quote="$pig$"
	}={}) {
		if(value==null) {
			return "NULL";
		} else {
			return dollar
				? `${quote}${value}${quote}`
				: value.toString().replace(/'/g, "''");
		}
	}
};

exports.fragment=fragment;
exports.value=value;
