/**
 * User: curtis
 * Date: 9/20/18
 * Time: 9:20 PM
 * Copyright @2018 by Xraymen Inc.
 */

const constant=require("../../../common/constant");

exports.type={
	/**
	 * Maps pig types to SQL types
	 */
	fromPig: {
		[constant.field.type.BOOLEAN]: "BOOLEAN",
		[constant.field.type.DATE]: "TIMESTAMP",
		[constant.field.type.ENUM]: "TEXT",
		[constant.field.type.ID]: "BIGINT",
		[constant.field.type.INTEGER]: "BIGINT",
		[constant.field.type.NUMBER]: "DOUBLE",
		[constant.field.type.OBJECT]: "JSON",
		[constant.field.type.STRING]: "TEXT"
	}
};

exports.operator={
	/**
	 * Maps pig operators to SQL operators
	 */
	fromPig: {
		[constant.request.param.operation.EQUAL]: "=",
		[constant.request.param.operation.NOT_EQUAL]: "!=",
		[constant.request.param.operation.GREATER_THAN]: ">",
		[constant.request.param.operation.GREATER_THAN_EQ]: ">=",
		[constant.request.param.operation.LESS_THAN]: "<",
		[constant.request.param.operation.LESS_THAN_EQ]: "<="
	}
};

