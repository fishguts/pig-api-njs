/**
 * User: curtis
 * Date: 9/20/18
 * Time: 10:27 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const map=require("./map");

/**
 * Creates fully executable SQL statements
 */
class SQLStatementFactory {
	/**
	 * @param {string} table
	 */
	constructor(table) {
		this._table=table;
		/**
		 * @type {Array<*>}
		 */
		this._params=undefined;
	}

	/**
	 * Counts the number of documents matching the query
	 * @param {DatabaseFilters} filters
	 * @returns {{params:Array<*>, text:string}}
	 */
	count(filters) {
		this._params=[];
		const selector=this._filtersToSelector(filters);
		let text=`SELECT 1 FROM "${this._table}"`;
		if(selector.length>0) {
			text=`${text} WHERE ${selector}`;
		}
		return {
			text: text,
			params: this._params
		};
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {{params:Array<*>, text:string}}}
	 */
	createMany({attributes, data}) {
		this._params=[];
		const valueFragment=this._dataToValuesFragment(data, true),
			returnColumns=this._attributesToColumnString(attributes);
		let text=`INSERT INTO "${this._table}" ${valueFragment}`;
		if(returnColumns.length>0) {
			// strange - if I wrap fields in parens it causes the results to be wrapped in parens almost acting like a format spec.
			// Not understanding but not important. Just don't wrap.
			text=`${text} RETURNING ${returnColumns}`;
		}
		return {
			text,
			params: this._params
		};
	}

	/**
	 * Deletes 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {{params:Array<*>, text:string}}}
	 */
	deleteMany({attributes, filters}) {
		this._params=[];
		const selector=this._filtersToSelector(filters);
		let text=`DELETE FROM "${this._table}"`;
		if(selector.length>0) {
			text=`${text} WHERE ${selector}`;
		}
		return {
			text: text,
			params: this._params
		};
	}

	/**
	 * Gets distinct values for the response field specified in attributes after filtered by filter if there is one.
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {{params:Array<*>, text:string}}}
	 */
	distinct({attributes, filters}) {
		this._params=[];
		const selector=this._filtersToSelector(filters),
			columns=this._attributesToColumnString(attributes);
		let text=`SELECT DISTINCT ${columns} FROM "${this._table}"`;
		if(selector.length>0) {
			text=`${text} WHERE ${selector}`;
		}
		return {
			text: text,
			params: this._params
		};
	}

	/**
	 * Find 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {{params:Array<*>, text:string}}}
	 */
	findMany({attributes, filters}) {
		this._params=[];
		const selector=this._filtersToSelector(filters),
			columns=this._attributesToColumnString(attributes),
			sort=this._attributesToSortString(attributes);
		let text=`SELECT ${columns} FROM "${this._table}"`;
		if(selector.length>0) {
			text=`${text} WHERE ${selector}`;
		}
		if(attributes.limit) {
			text=`${text} LIMIT ${attributes.limit.value}`;
		}
		if(attributes.skip) {
			text=`${text} OFFSET ${attributes.skip.value}`;
		}
		if(sort.length>0) {
			text=`${text} ORDER BY ${sort}`;
		}
		return {
			text: text,
			params: this._params
		};
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the documents that should be updated
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @returns {{params:Array<*>, text:string}}}
	 */
	updateMany({
		attributes, data, filters,
		retrieve=true,
		upsert=false
	}) {
		this._params=[];
		const returnColumns=this._attributesToColumnString(attributes),
			selector=this._filtersToSelector(filters),
			setFragment=this._dataToColumnValuePairs(data)
				.map(pair=>`"${pair.column}"=${this._addParam(pair.value)}`)
				.join(", ");
		let text=`UPDATE "${this._table}" SET ${setFragment}`;
		if(selector.length>0) {
			text=`${text} WHERE ${selector}`;
		}
		if(returnColumns.length>0) {
			text=`${text} RETURNING ${returnColumns}`;
		}
		return {
			text: text,
			params: this._params
		};
	}

	/**************** Private Interface ****************/
	/**
	 * Adds param to our queue and returns substitution markup
	 * @param {*} param
	 * @return {string}
	 * @private
	 */
	_addParam(param) {
		this._params.push(param);
		return `$${this._params.length}`;
	}

	/**
	 * Converts <code>attributes.fields</code> into a comma delimited string if there are fields in attributes
	 * @param {DatabaseAttributes} attributes
	 * @returns {string}
	 * @private
	 */
	_attributesToColumnString(attributes) {
		return (attributes.fields)
			? attributes.fields.value
				.map(name=>`"${name}"`)
				.join(",")
			: "";
	}

	/**
	 * Converts <code>attributes.fields</code> into a comma delimited string if there are fields in attributes
	 * @param {DatabaseAttributes} attributes
	 * @returns {string}
	 * @private
	 */
	_attributesToSortString(attributes) {
		return (attributes.sort)
			? attributes.sort.value
				.map(property=>property.startsWith("-")
					? `"${property}" ASC`
					: `"${property}" DESC`
				).join(",")
			: "";
	}


	/**
	 * Reduces data to an array of model-paths/columns
	 * @param {DatabaseData} data
	 * @returns {Array<string>}
	 * @private
	 */
	_dataToColumns(data) {
		return _.chain(data.params)
			.reduce((result, param)=> {
				return result.concat(_.map(param["x-pig"].flattened, field=> {
					const depth=(field.path.match(/\./g) || []).length;
					return {
						sort: `${depth}.${field.path}`,
						path: field.path
					};
				}));
			}, [])
			.sortBy("sort")
			.sortedUniqBy("sort")
			.map("path")
			.value();
	}

	/**
	 * Creates an array of column|value pairs.
	 * @param {DatabaseData} data
	 * @returns {Array<{column:string, value:*}>}
	 * @private
	 */
	_dataToColumnValuePairs(data) {
		const paths=this._dataToColumns(data);
		return paths.map(path=>({
			column: path,
			value: _.get(data, path, null)
		}));
	}

	/**
	 * Creates insert/update VALUES fragment: (<columns>) VALUES (<params>)[,(<params>)...]
	 * @param {DatabaseData} data
	 * @param {boolean} scalar - whether data.value should be treated as a scalar or single value
	 * @returns {string}
	 * @private
	 */
	_dataToValuesFragment(data, scalar) {
		const paths=this._dataToColumns(data),
			columns=`(${paths.map(path=>`"${path}"`).join(",")})`;
		const _objectToColumnValues=(object)=> {
			const values=paths.map(property=> {
				return this._addParam(_.get(object, property, null));
			});
			return `(${values.join(",")})`;
		};
		if(scalar) {
			const values=_.map(data.value, _objectToColumnValues);
			return `${columns} VALUES ${values.join(", ")}`;
		} else {
			return `${columns} VALUES ${_objectToColumnValues(data.value)}`;
		}
	}

	/**
	 * Translates a mapping of filters into a query selector
	 * @param {DatabaseFilters} filters
	 * @return {string}
	 * @private
	 */
	_filtersToSelector(filters) {
		return filters.map(filter=> {
			const operator=map.operator.fromPig[filter.operation] || filter.operation;
			return `"${filter.path}"${operator}${this._addParam(filter.value)}`;
		}).join(" AND ");
	}
}

exports.StatementFactory=SQLStatementFactory;
