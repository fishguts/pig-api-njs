/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const {ServiceBase}=require("../_base");

/**
 * Base class for any collection of data. I borrowed mongo's term "collection". It could be thought of as a "table".
 *  Note: We are going to try and abstract the implementation. These things don't always go well so
 *  we will allow for a collection to expose the database interface itself but let's try and stay away
 *  from it as our db techs are not set in stone right now and for our ddm layer should be flexible.
 * @abstract
 * @typedef {ServiceBase} DatabaseCollection
 */
class DatabaseCollection extends ServiceBase {
	/**** Abstract/Virtual Interface: Initialization ****/
	/**
	 * Chance to initialize this collection. Does nothing by default
	 * @returns {Promise}
	 */
	async initialize() {
		return super.initialize();
	}

	/**** Abstract/Virtual Interface: IO *****/
	/**
	 * Counts the number of documents matching the query
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseCountResult>}
	 * @abstract
	 */
	count(filters) {
		throw new Error("abstract");
	}

	/**
	 * Inserts a new document
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {Promise<DatabaseCreateOneResult>}
	 */
	createOne({
		attributes,
		data
	}) {
		return this.createMany({
			data: {
				value: [data.value],
				params: data.params
			},
			attributes
		}).then(results=>_.get(results, 0, null));
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {Promise<DatabaseCreateManyResult>}
	 * @abstract
	 */
	createMany({
		attributes,
		data
	}) {
		throw new Error("abstract");
	}

	/**
	 * Deletes single document
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {boolean} failNotFound - reject if document is not found
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteOneResult>}
	 */
	deleteOne({
		attributes,
		failNotFound=true,
		filters
	}) {
		return this.deleteMany({attributes, filters})
			.then(result=> {
				if(failNotFound && result.count===0) {
					throw this._createError({
						message: `${this.name} document not found`,
						filters: filters,
						statusCode: http.status.code.NOT_FOUND
					});
				} else {
					return result;
				}
			});
	}

	/**
	 * Deletes 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteManyResult>}
	 * @abstract
	 */
	deleteMany({
		attributes,
		filters
	}) {
		throw new Error("abstract");
	}

	/**
	 * Gets distinct values for the response field specified in attributes after filtered by filter if there is one.
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDistinctResult>}
	 * @abstract
	 */
	distinct({
		attributes,
		filters
	}) {
		throw new Error("abstract");
	}

	/**
	 * Find a single document
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @param {boolean} failNotFound
	 * @returns {Promise<DatabaseFindOneResult>}
	 */
	findOne({
		attributes,
		filters,
		failNotFound=true
	}) {
		return this.findMany({attributes, filters})
			.then(documents=> {
				if(_.size(documents)>0) {
					return documents[0];
				} else if(failNotFound) {
					throw this._createError({
						message: `${this.name} document not found`,
						filters: filters,
						statusCode: http.status.code.NOT_FOUND
					});
				} else {
					return null;
				}
			});
	}

	/**
	 * Find 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseFindManyResult>}
	 * @abstract
	 */
	findMany({
		attributes,
		filters
	}) {
		throw new Error("abstract");
	}

	/**
	 * Updates a single document identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the document that should be updated
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @returns {Promise<DatabaseUpdateOneResult>}
	 */
	updateOne({
		attributes,
		data,
		filters,
		retrieve=true,
		upsert=false
	}) {
		// note: if the DB does not have control over the number of objects updated then allowing this
		// to be forwarded to updateMany will have no effect. And if it does have control over how many
		// objects are updated and multiple objects are eligible? Then something isn't right with the request.
		return this.updateMany({
			attributes,
			data: {
				data: [data.value],
				params: data.params
			},
			filters,
			retrieve,
			upsert
		});
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the documents that should be updated
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @returns {Promise<DatabaseUpdateManyResult>}
	 * @abstract
	 */
	updateMany({
		attributes,
		data,
		filters,
		retrieve=true,
		upsert=false
	}) {
		throw new Error("abstract");
	}

	/**** Protected Support ****/
	/**
	 * Create a PigError from another error
	 * @param {Object} mixins
	 * @param {Error} error
	 * @returns {exports.PigError}
	 * @protected
	 */
	_createError(mixins, error=undefined) {
		if(error) {
			return new PigError(Object.assign({
				instance: this,
				collection: this.name,
				message: error.message,
				error: error
			}, mixins));
		} else {
			return new PigError(Object.assign({
				instance: this,
				collection: this.name
			}, mixins));
		}
	}
}

exports.DatabaseCollection=DatabaseCollection;
