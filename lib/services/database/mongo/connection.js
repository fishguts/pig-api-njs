/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const {MongoSetup}=require("./_setup");
const {ServiceBase}=require("../../_base");

/**
 * Master and commander of a mongo connection.
 * @typedef {ServiceBase} MongoConnection
 */
class MongoConnection extends ServiceBase {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		super(configuration);
		/**
		 * @type {MongoSetup}
		 */
		this._setup=null;
	}

	/**
	 * Accessor for our mongo database interface
	 * @returns {Db}
	 */
	get database() {
		return this.setup.mongoDatabase;
	}

	/**
	 * Accessor a fully configured mongo setup
	 * @returns {MongoSetup}
	 */
	get setup() {
		assert.toLog(this._setup, "accessing setup before it is set?");
		return this._setup;
	}

	/**** Implementation of ServiceBase functionality ****/
	/**
	 * Sets up this database for action
	 * @returns {Promise}
	 */
	async initialize() {
		assert.toLog(this._setup===null);
		this._setup=new MongoSetup(this.configuration);
		return this._setup.execute();
	}

	/**
	 * Closes the client connection
	 * @returns {Promise}
	 */
	async shutdown() {
		if(!this._setup) {
			return Promise.resolve();
		} else {
			const setup=this._setup;
			this._setup=null;
			return setup.mongoClient.close();
		}
	}
}

exports.MongoConnection=MongoConnection;
