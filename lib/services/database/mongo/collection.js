/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const mongodb=require("mongodb");
const http=require("pig-core").http;
const util=require("pig-core").util;
const constant=require("../../../common/constant");
const {DatabaseCollection}=require("../collection");

/**
 * Interface to a mongo collection.
 * @typedef {DatabaseCollection} MongoCollection
 */
class MongoCollection extends DatabaseCollection {
	/**************** Internal type definitions ****************/
	/**
	 * @typedef {Object} MongoCollectionAttributes
	 * @property {Number|undefined} limit
	 * @property {Object<string, number>|undefined} projection
	 * @property {Number|undefined} skip
	 * @property {Array<string>|undefined} sort
	 */

	/**************** Public Interface ****************/
	/**
	 * @param {SpecDatabaseCollection} configuration
	 * @param {MongoConnection} connection
	 */
	constructor(configuration, connection) {
		super(configuration);
		this._connection=connection;
		/**
		 * @type {Collection}
		 */
		this._collection=null;
	}

	/**
	 * Sets this collection up for action
	 * @returns {Promise}
	 */
	initialize() {
		this._collection=this._connection.setup.mongoCollectionMap[this.configuration.id];
		return this._collection
			? Promise.resolve()
			: Promise.reject(this._createError({
				details: `id=${this.configuration.id}`,
				message: "unexpectedly missing mongo collection"
			}));
	}

	/**** Interface definition ****/
	/**
	 * Counts the number of documents matching the query
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseCountResult>}
	 */
	count(filters) {
		const filter=this._filtersToSelector(filters);
		return this._collection.countDocuments(filter);
	}

	/**
	 * Inserts 1 or more new documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseData} data
	 * @returns {Promise<DatabaseCreateManyResult>}
	 */
	createMany({
		attributes,
		data
	}) {
		/**
		 * Ch
		 * @param {Array<Object>} result
		 * @returns {null|Array<Object>}
		 */
		function _resultToResponse(result) {
			if(!attributes.fields) {
				return null;
			} else {
				return util.arrayPick(result.ops, attributes.fields.value);
			}
		}

		const options=this._attributesToOptions(attributes);
		return this._collection.insertMany(data.value, options)
			.then(_resultToResponse)
			.catch((error)=>{
				return (error.code===11000)
					? this._createError({
						ids: _.get(error.message.match(/{\s+:\s+([^}]+)/), 1, "")
							.replace(/"/g, "").trim(),
						message: "id already exists",
						statusCode: http.status.code.CONFLICT
					}, error)
					: error;
			});
	}

	/**
	 * Deletes 1 document
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {boolean} failNotFound - reject if document is not found
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteOneResult>}
	 */
	deleteOne({
		attributes,
		failNotFound=true,
		filters
	}) {
		const options=this._attributesToOptions(attributes),
			filter=this._filtersToSelector(filters);
		return this._collection.deleteOne(filter, options)
			.then(result=>{
				if(failNotFound && result.deletedCount<1) {
					throw this._createError({
						message: `${this.name} document not found`,
						filters: filters,
						statusCode: http.status.code.NOT_FOUND
					});
				} else {
					return {deleted: result.deletedCount};
				}
			});
	}

	/**
	 * Deletes 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDeleteManyResult>}
	 */
	deleteMany({
		attributes,
		filters
	}) {
		const options=this._attributesToOptions(attributes),
			filter=this._filtersToSelector(filters);
		return this._collection.deleteMany(filter, options)
			.then(result=>({deleted: result.deletedCount}));
	}

	/**
	 * Gets distinct values for the response field specified in attributes after filtered by filter if there is one.
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseDistinctResult>}
	 */
	distinct({
		attributes,
		filters
	}) {
		const fieldName=attributes.fields.value[0],
			filter=this._filtersToSelector(filters);
		return this._collection.distinct(fieldName, filter);
	}


	/**
	 * Find 0 or more documents
	 * @param {DatabaseAttributes} attributes - such as fields to return
	 * @param {DatabaseFilters} filters
	 * @returns {Promise<DatabaseFindManyResult>}
	 */
	findMany({
		attributes,
		filters
	}) {
		const options=this._attributesToOptions(attributes),
			query=this._filtersToSelector(filters);
		return this._collection.find(query, options).toArray();
	}

	/**
	 * Updates a single document identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the document that should be updated
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @returns {Promise<DatabaseUpdateOneResult>}
	 */
	updateOne({
		attributes,
		data,
		filters,
		upsert=false,
		retrieve=true
	}) {
		const options=this._attributesToOptions(attributes),
			selector=this._filtersToSelector(filters),
			operation={"$set": data.value};
		options.upsert=upsert;
		if(retrieve) {
			return this._collection.findOneAndUpdate(selector, operation, options)
				.then(result=>{
					if(result.lastErrorObject.n===0) {
						throw this._createError({
							selector: selector,
							statusCode: http.status.code.NOT_FOUND
						});
					} else {
						return result.value;
					}
				});
		} else {
			return this._collection.updateOne(selector, operation, options)
				.then(result=>({updated: result.modifiedCount}));
		}
	}

	/**
	 * Updates 0 or more documents identified by selector
	 * @param {DatabaseAttributes} attributes
	 * @param {DatabaseData} data
	 * @param {DatabaseFilters} filters - isolate the documents that should be updated
	 * @param {boolean} retrieve - whether to retrieve the updated document
	 * @param {boolean} upsert - whether to insert the document if it does not exist
	 * @returns {Promise<DatabaseUpdateManyResult>}
	 */
	updateMany({
		attributes,
		data,
		filters,
		retrieve=true,
		upsert=false
	}) {
		const options=this._attributesToOptions(attributes),
			selector=this._filtersToSelector(filters),
			operation={"$set": data.value};
		options.upsert=upsert;
		if(retrieve) {
			return this._collection.updateMany(selector, operation, options)
				.then(()=>this.findMany({
					attributes,
					filters
				}));
		} else {
			return this._collection.updateMany(selector, operation, options)
				.then(result=>({updated: result.modifiedCount}));
		}
	}

	/**************** Private Interface ****************/
	/**
	 * translate a DatabaseFilters object to a mongoose query object
	 * @param {DatabaseAttributes} attributes
	 * @returns {MongoCollectionAttributes}
	 * @private
	 */
	_attributesToOptions(attributes) {
		// flatten him
		const translate={
				"fields": "projection"
			},
			result=_.reduce(attributes, (result, attribute, key)=>{
				result[translate[key] || key]=attribute.value;
				return result;
			}, {});
		if(result.projection) {
			result.projection=result.projection.reduce((result, value)=>{
				if(_.startsWith(value, "-")) {
					result[value.substr(1)]=0;
				} else {
					result[value]=1;
				}
				return result;
			}, {});
		}
		if(result.sort) {
			result.sort=result.sort.map(function(value) {
				return (_.startsWith(value, "-"))
					? [value.substr(1), -1]
					: [value, 1];
			});
		}
		return result;
	}

	/**
	 * translate a DatabaseFilters object to a mongoose query object
	 * @param {DatabaseFilters} filters
	 * @returns {Object}
	 * @private
	 */
	_filtersToSelector(filters) {
		return _.reduce(filters, (result, filter)=>{
			/**
			 * 1. first we are going to see whether we need to transform the filter value.
			 */
			let value=filter.value;
			if(_.includes(filter.param["x-pig"].attributes, constant.field.attribute.AUTO_ID)) {
				try {
					value=new mongodb.ObjectID(value);
				} catch(e) {
					// hmmmm, he's not an id?  Then we assume the user is using is own ids. We leave it.
				}
			}

			/**
			 * 2. Now we translate the filter operation into it's jSON equivelent for mongo filtering
			 */
			switch(filter.operation) {
				case constant.request.param.operation.NOT_EQUAL: {
					result[filter.path]={$ne: value};
					break;
				}
				case constant.request.param.operation.GREATER_THAN: {
					result[filter.path]={$gt: value};
					break;
				}
				case constant.request.param.operation.GREATER_THAN_EQ: {
					result[filter.path]={$gte: value};
					break;
				}
				case constant.request.param.operation.LESS_THAN: {
					result[filter.path]={$lt: value};
					break;
				}
				case constant.request.param.operation.LESS_THAN_EQ: {
					result[filter.path]={$lte: value};
					break;
				}
				default: {
					result[filter.path]=value;
				}
			}
			return result;
		}, {});
	}
}

exports.MongoCollection=MongoCollection;
