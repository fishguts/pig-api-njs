/**
 * User: curtis
 * Date: 9/1/18
 * Time: 6:03 PM
 * Copyright @2018 by Xraymen Inc.
 */

const mongodb=require("mongodb");
const {PigError}=require("pig-core").error;
const log=require("pig-core").log;
const {DatabaseRegistry}=require("../_registry");

class MongoSetup {
	/**
	 * @param {SpecService} configuration
	 */
	constructor(configuration) {
		this._configuration=configuration;
		/**
		 * @type {MongoClient}
		 */
		this.mongoClient=null;
		/**
		 * @type {Db}
		 */
		this.mongoDatabase=null;
		/**
		 * @type {Object<string, Collection>}
		 */
		this.mongoCollectionMap={};
	}

	/**
	 * Sets up our database which includes:
	 * - create connection to database
	 * - making sure this service's model exists in the form of collections
	 * - makes connections to each collection
	 * @returns {Promise<void>}
	 */
	async execute() {
		const registry=new DatabaseRegistry(this._configuration);
		return this._initialize()
			.then(registry.initialize.bind(registry, {
				load: this._loadRegistryData.bind(this),
				save: this._saveRegistryData.bind(this)
			}))
			.then(this._setupDatabase.bind(this, registry))
			.then(this._setupCollections.bind(this, registry))
			.then(this._setupIndices.bind(this, registry))
			.then(registry.complete.bind(registry));
	}

	/**************************** Private Interface ****************************/
	/**
	 * Connects to the mongo server.
	 * @returns {Promise<void>}
	 * @private
	 */
	_initialize() {
		const connectString=this._configuration.database.connectionString;
		return mongodb.MongoClient.connect(connectString, {useNewUrlParser: true})
			.then((client)=>{
				log.verbose(`MongoSetup._createConnection(): connection to ${connectString} succeeded`);
				this.mongoClient=client;
			})
			.catch((error)=>{
				throw new PigError({
					instance: this,
					error: error,
					message: "failed to connect to mongo server"
				});
			});
	}

	/**
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @return {Promise<Object>}
	 * @private
	 */
	_loadRegistryData(databaseName, collectionId, dataId) {
		return this.mongoClient.db(databaseName)
			.createCollection(collectionId)
			.then(collection=>
				collection.findOne({_id: dataId})
			);
	}

	/**
	 * @param {string} databaseName
	 * @param {string} collectionId
	 * @param {string} dataId
	 * @param {Object} data
	 * @return {Promise<void>}
	 * @private
	 */
	_saveRegistryData(databaseName, collectionId, dataId, data) {
		return this.mongoClient.db(databaseName)
			.createCollection(collectionId)
			.then(collection=>
				collection.findOneAndReplace({_id: dataId}, data, {upsert: true})
			);
	}

	/**
	 * @param {DatabaseRegistry} registry
	 * @returns {Promise<void>}
	 */
	_setupDatabase(registry) {
		const connect=(id, name, options)=>{
				this.mongoDatabase=this.mongoClient.db(name, options);
				return Promise.resolve();
			},
			rename=(nameFrom, nameTo)=>{
				throw new Error("how to rename?");
			};
		return registry.setupDatabase({
			connect: connect,
			create: connect,
			rename
		});
	}

	/**
	 * @param {DatabaseRegistry} registry
	 * @returns {Promise<void>}
	 */
	_setupCollections(registry) {
		const create=(id, name, options)=>{
				return this.mongoDatabase.createCollection(name, options)
					.then(mongoCollection=>{
						this.mongoCollectionMap[id]=mongoCollection;
					});
			},
			// we are schema-less. Not much of a concern for us yet.
			refactor=()=>Promise.resolve(),
			rename=(nameFrom, nameTo)=>{
				return this.mongoDatabase.renameCollection(nameFrom, nameTo);
			};
		return registry.setupCollections({
			connect: create,
			create: create,
			refactor,
			rename
		});
	}

	/**
	 * @param {DatabaseRegistry} registry
	 * @returns {Promise<void>}
	 */
	async _setupIndices(registry) {
		const create=(collectionId, collectionName, index)=>{
				const mongoCollection=this.mongoCollectionMap[collectionId];
				return mongoCollection.createIndex(index.spec, Object.assign({
					name: index.name
				}, index.options));
			},
			remove=(collectionId, collectionName, index)=>{
				const mongoCollection=this.mongoCollectionMap[collectionId];
				return mongoCollection.dropIndex(index.name);
			};
		return registry.setupIndices({
			create,
			remove
		});
	}
}

exports.MongoSetup=MongoSetup;
