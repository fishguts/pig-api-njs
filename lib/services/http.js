/**
 * User: curtis
 * Date: 2/21/18
 * Time: 11:07 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const request=require("request");
const {PigError}=require("pig-core").error;
const {ServiceBase}=require("./_base");

/**
 * Handles an http/https request.
 * - requires no initialization
 * @typedef {ServiceBase} HttpService
 */
class HttpService extends ServiceBase {
	/**
	 * @param {IncommingRequest} _request
	 * @returns {Promise}
	 */
	send(_request) {
		// note: at this point we are going to assume that we are validated
		return new Promise((resolve, reject)=> {
			const options=this._buildRequestOptions(_request);
			request(options, (error, incomming)=> {
				if(error) {
					reject(new PigError({
						error,
						instance: this,
						url: options.uri
					}));
				} else if(incomming.statusCode>=400) {
					const errorProperties=_.isPlainObject(incomming.body)
						? incomming.body
						: {};
					errorProperties.url=options.uri;
					errorProperties.statusCode=incomming.statusCode;
					errorProperties.statusMessage=incomming.statusMessage;
					if(!errorProperties.message) {
						errorProperties.message=`failed: ${incomming.statusCode} - ${incomming.statusMessage}`;
					}
					// we are sort of assuming that the details are our own. But
					reject(new PigError(errorProperties));
				} else {
					resolve(_request.stack.push({
						contentType: incomming.headers["content-type"],
						result: incomming.body,
						statusCode: incomming.statusCode
					}));
				}
			});
		});
	}

	/**
	 * This guy mashes up our configuration with the incomming-request.
	 * Substitutions: the way they work is based on our stack.  We give access to all properties in stack: <code>params, body, result</code>
	 * @param {IncommingRequest} _request
	 * @returns {Object}
	 */
	_buildRequestOptions(_request) {
		const settings=this.getProperty("request"),
			params=_request.stack.get(),
			interpolate=/{{\s*([\S]+)\s*}}/g;
		return _.omitBy({
			contentType: _.get(settings, "contentType", "application/json"),
			method: _.get(settings, "method", "GET"),
			uri: _.template(settings.uri, {interpolate})(params),
			headers: (settings.headers)
				? _.mapValues(settings.headers, (value)=>_.template(value, {interpolate})(params))
				: undefined,
			body: _request.stack.body
		}, _.isUndefined);
	}
}

exports.HttpService=HttpService;
