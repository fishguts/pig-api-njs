/**
 * User: curtis
 * Date: 2/26/18
 * Time: 9:20 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const util=require("pig-core").util;
const {ServiceBase}=require("./_base");

/**
 * Handles one or more data transformation requests
 * - requires no initialization
 * @typedef {ServiceBase} TransformService
 */
class TransformService extends ServiceBase {
	/**
	 * Applies edits to the specified request
	 * @param {IncommingRequest} request
	 * @param {[String]} commands
	 * @returns {Promise<{body:Object, result:Object, contentType:String, params:Object, statusCode:Number}>}
	 */
	execute(request, commands) {
		let data=request.stack.get(true);
		commands.forEach((command)=> {
			command=command.trim();
			const tokens=command.split(/\s+/),
				args=tokens.slice(1);
			switch(tokens[0]) {
				case "copy":
					data=this._copy(data, args, command);
					break;
				case "delete":
					data=this._delete(data, args, command);
					break;
				default: {
					throw new PigError({
						instance: this,
						message: `unknown data transformation '${tokens[0]}'`,
						statusCode: http.status.code.BAD_REQUEST
					});
				}
			}
		});
		return Promise.resolve(request.stack.push(data));
	}

	/**** Private Interface ****/
	/**
	 * Translates the specified path into an absolute data path.
	 * @param {string} path
	 * @returns {string}
	 */
	static toAbsPath(path) {
		if(_.startsWith(path, "params.")) {
			return path;
		}
		return `params.${path}`;
	}

	/**
	 * Asserts expected arguments
	 * @param {Array} args
	 * @param {Number} count
	 * @param {string} command
	 * @throws {Error}
	 */
	assertArguments(args, count, command) {
		if(args.length!==count) {
			throw new PigError({
				instance: this,
				detail: command,
				message: `expected ${count} arguments but found ${args.length}`,
				statusCode: http.status.code.BAD_REQUEST
			});
		}
	}

	/**
	 * Copies data
	 * @param {{body:Object, params:Object}} data
	 * @param {[String]} args
	 * @param {string} command
	 * @returns {{body:Object, result:Object, contentType:String, params:Object, statusCode:Number}}
	 * @private
	 */
	_copy(data, args, command) {
		this.assertArguments(args, 2, command);
		args=args.map(TransformService.toAbsPath);
		return _.set(data, args[1], _.get(args, args[2]));
	}

	/**
	 * Deletes data
	 * @param {{body:Object, params:Object}} data
	 * @param {[String]} args
	 * @param {string} command
	 * @returns {{body:Object, result:Object, contentType:String, params:Object, statusCode:Number}}
	 * @private
	 */
	_delete(data, args, command) {
		this.assertArguments(args, 1, command);
		return util.delete(data, TransformService.toAbsPath(args[0]));
	}
}

exports.TransformService=TransformService;
