/**
 * User: curtis
 * Date: 2/8/18
 * Time: 9:48 PM
 * Copyright @2018 by Xraymen Inc.
 */

const pig_core=require("pig-core");
const log=require("pig-core").log;
const notification=require("pig-core").notification;
const constant=require("./common/constant");
const mime=require("./common/mime");
const model_factory=require("./model/factory");
const route=require("./route");
const services=require("./services");

const api={
	constant: constant,
	error: pig_core.error,
	file: pig_core.file,
	log: {
		debug: pig_core.log.debug,
		verbose: pig_core.log.verbose,
		info: pig_core.log.info,
		warn: pig_core.log.warn,
		error: pig_core.log.error,
		level: pig_core.log.level,
		middleware: log.middleware
	},
	mime: mime,
	notification: pig_core.notification,
	route: route,
	util: pig_core.util,

	/**
	 * Gives access to the application configuration. It is configured and set during <code>initialize</code>
	 * @type {Application}
	 */
	application: null,
	/**
	 * Access to instance created during <code>initialize</code>
	 * @type {Specification}
	 */
	specification: null,

	/**
	 * Creates an instance of Specification from <code>apiUri</code> which should be the openAPI definition of the current project.
	 * @param {Object|String} apiUri if it is a string then it will be treated as either a path (if it is a directory) or a full path to the spec.
	 * @returns {Promise<Specification>}
	 */
	initialize(apiUri) {
		return new Promise(function(resolve) {
			api.specification=model_factory.specification.set(apiUri);
			api.application=model_factory.application.get();
			resolve(services.initialize(api.specification));
		})
			.then(()=>api.specification)
			.catch((error)=> {
				log.error(error);
				throw error;
			});
	},

	/**
	 * Attempts to gracefully shut the machine down
	 * @returns {Promise<void>}
	 */
	shutdown() {
		return services.shutdown();
	}
};

module.exports=api;
