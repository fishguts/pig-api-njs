/**
 * User: curtis
 * Date: 2/14/18
 * Time: 12:17 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {PigError}=require("pig-core").error;
const http=require("pig-core").http;

/**
 * Base class handler for all routes
 */
class RouteHandler {
	/**
	 * @param {IncommingRequest} request incomming request
	 * @param {SpecPathOperation} operation
	 * @param {ServiceBase} service
	 */
	constructor(request, operation, service) {
		this._request=request;
		this._operation=operation;
		this._service=service;
	}

	/**
	 * @returns {SpecPathOperation}
	 */
	get operation() {
		return this._operation;
	}
	/**
	 * @returns {IncommingRequest}
	 */
	get request() {
		return this._request;
	}
	/**
	 * @returns {ServiceBase}
	 */
	get service() {
		return this._service;
	}

	/**
	 * Asserts that our service is an instance of a particular type
	 * @param {Function} type
	 */
	assertServiceType(type) {
		if(!(this._service instanceof type)) {
			throw new PigError({
				instance: this,
				details: `expecting '${type.name}'`,
				message: "improper service type",
				statusCode: http.status.code.BAD_REQUEST
			});
		}
	}
}

exports.Handler=RouteHandler;
