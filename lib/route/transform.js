/**
 * User: curtis
 * Date: 2/14/18
 * Time: 11:51 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {Handler}=require("./_base");
const {TransformService}=require("../services/transform");

/**
 * Delegates transform requests to the service
 * @typedef {Handler} TransformRouteHandler
 */
class TransformRouteHandler extends Handler {
	/**
	 * Processes the route and returns a promise that is fulfilled with the service's result
	 * @returns {Promise<any>}
	 */
	process() {
		this.assertServiceType(TransformService);
		// because edits are so granular we allow for there to be one or more operations.
		const edits=_.isArray(this.operation.type)
			? this.operation.type
			: [this.operation.type];
		return this.service.execute(this.request, edits);
	}
}


exports.Handler=TransformRouteHandler;
