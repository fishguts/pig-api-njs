/**
 * User: curtis
 * Date: 2/14/18
 * Time: 11:39 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const {Handler}=require("./_base");
const pig_core=require("pig-core");
const constant=require("../common/constant");
const model_factory=require("../model/factory");
const {DatabaseCollection}=require("../services/database/collection");

/**
 * Attempts to perform database operations automatically.
 * @typedef {Handler} DatabaseRouteHandler
 */
class DatabaseRouteHandler extends Handler {
	/**
	 * @param {IncommingRequest} request incomming request
	 * @param {SpecPathOperation} operation
	 * @param {ServiceBase} service
	 */
	constructor(request, operation, service) {
		super(request, operation, service);
		/**
		 * Array of ids that are part of our API. They are not fields that our database will know about.
		 * @type {Array<string>}
		 */
		this._internalResultFields=_.chain(model_factory.gamuts.get().model.fieldReferences)
			.filter({propertyOf: constant.field.propertyOf.RESULT})
			.map("fieldId")
			.value();
	}

	/**
	 * Processes the route and calls back with result
	 * @returns {Promise<any>}
	 */
	process() {
		try {
			this.assertServiceType(DatabaseCollection);
			const {attributes, data, filters}=this._openApiToInternal();
			switch(this.operation.type) {
				case "count": {
					return this.service.count(filters)
						.then(result=>this.request.stack.push({result}));
				}
				case "create-one": {
					return this.service.createOne({attributes, data})
						.then(result=>this.request.stack.push({result}));
				}
				case "create-many": {
					return this.service.createMany({attributes, data})
						.then(result=>this.request.stack.push({result}));
				}
				case "delete-one": {
					return this.service.deleteOne({attributes, filters})
						.then(result=>this.request.stack.push({result}));
				}
				case "delete-many": {
					return this.service.deleteMany({attributes, filters})
						.then(result=>this.request.stack.push({result}));
				}
				case "distinct": {
					pig_core.validator.validateData({attributes, filters}, "./res/schema/database-distinct-params.json");
					return this.service.distinct({attributes, filters})
						.then(result=>this.request.stack.push({result}));
				}
				case "get-one": {
					return this.service.findOne({attributes, filters})
						.then(result=>this.request.stack.push({result}));
				}
				case "get-many": {
					return this.service.findMany({attributes, filters})
						.then(result=>this.request.stack.push({result}));
				}
				case "update-one": {
					// todo: we should look at the response and determine whether they want data back or not
					return this.service.updateOne({
						attributes,
						data,
						filters,
						retrieve: _.isEmpty(this.request.extensions.response.fields)===false,
						upsert: false
					}).then(result=>this.request.stack.push({result}));
				}
				case "upsert-one": {
					// todo: we should look at the response and determine whether they want data back or not
					return this.service.updateOne({
						attributes,
						data,
						filters,
						retrieve: _.isEmpty(this.request.extensions.response.fields)===false,
						upsert: true
					}).then(result=>this.request.stack.push({result}));
				}
				default: {
					throw new pig_core.error.PigError({
						instance: this,
						details: `unknown type '${this.operation.type}'`,
						message: "unknown database operation"
					});
				}
			}
		} catch(error) {
			return Promise.reject(error);
		}
	}

	/**** Private Interface ****/
	/**
	 * Picks apart the request and categorizes him and assembles him into attributes, data and filters.
	 * @returns {{attributes: DatabaseAttributes, data: DatabaseData, filters: DatabaseFilters}}
	 * @private
	 */
	_openApiToInternal() {
		const paramValueMap=this.request.stack.get().params,
			pathX=this.request.extensions,
			result={
				attributes: {
					fields: {
						value: _.chain(pathX.response.flattened)
							.filter(field=>!_.includes(this._internalResultFields, field.id))
							.map("path")
							.value()
					},
					sort: {
						value: []
					}
				},
				data: null,
				filters: []
			};

		/**
		 * @param {SpecParameter} param
		 */
		const apply=(param)=> {
			// note: param.name is the variable name and param["x-pig"].path should always be the property name. They may or may not be the same.
			const extension=param["x-pig"];
			let value=_.get(paramValueMap, param.name, param.default);
			if(extension.function===constant.request.param.type.QUERY) {
				if(extension.id==="urn:mdl:fld:fields" || extension.id==="urn:mdl:fld:sort") {
					value=(value || "").split(/\s*,\s*|\s+/);
				}
				result.attributes[extension.dataPath]={
					param: param,
					value: value
				};
			} else if(extension.function===constant.request.param.type.UPDATE) {
				if(result.data) {
					// this is a little weird - they have combined update data across multiple params. It's okay with us
					// but will most likely be very inaccurate if this request is an update
					result.update.value=Object.assign({}, result.update.value, value);
					this.update.params.push(param);
				} else {
					result.data={
						value: value,
						params: [param]
					};
				}
			} else if(extension.function===constant.request.param.type.FILTER) {
				// rules for filtering:
				// - body: we push the top tier of properties in as independent AND filters.
				// - path and query: they all get mixed together independently as AND filters.
				if(param.in===constant.request.param.location.BODY) {
					result.filters=_.reduce(value, (result, value, key)=> {
						result.push({
							operation: constant.request.param.operation.EQUAL,
							param: param,
							path: key,
							value: value
						});
						return result;
					}, result.filters);
				} else {
					result.filters.push({
						operation: extension.operation,
						param: param,
						path: extension.dataPath,
						value: value
					});
				}
			} else {
				pig_core.assert.toLog(false, new pig_core.error.PigError({
					instance: this,
					details: JSON.stringify(extension),
					message: "unknown parameter function"
				}));
			}
		};

		_.forEach(this.request.extensions.request.literals, apply);
		_.forEach(this.request.operation.parameters, apply);
		if(result.attributes.fields.value.length===0) {
			delete result.attributes.fields;
		}
		if(result.attributes.sort.value.length===0) {
			delete result.attributes.sort;
		}
		return result;
	}
}

exports.Handler=DatabaseRouteHandler;
