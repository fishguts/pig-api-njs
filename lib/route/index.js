/**
 * User: curtis
 * Date: 2/12/18
 * Time: 11:14 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const database=require("./database");
const http=require("./http");
const pig_core=require("pig-core");
const transform=require("./transform");
const constant=require("../common/constant");
const model_factory=require("../model/factory");
const services=require("../services");

/**
 * Processes the route and returns a promise
 * @param {Object} swaggerRequest route details.
 * @returns {Promise<{contentType:string, result:Object, statusCode:Number}>}
 * @throws {Error}
 */
exports.process=function(swaggerRequest) {
	let promiseChain;
	try {
		/* eslint-disable no-console */
		pig_core.log.verbose(JSON.stringify(swaggerRequest, null, "\t"));
		const incommingRequest=model_factory.incommingRequest.create(swaggerRequest),
			handlers=exports._requestToHandlers(incommingRequest);
		promiseChain=pig_core.promise.series(handlers.map((handler)=>handler.process.bind(handler)));
	} catch(error) {
		promiseChain=Promise.reject(error);
	}
	return promiseChain
		.then(result=>{
			return {
				contentType: result.contentType,
				result: result.result,
				statusCode: result.statusCode
			};
		})
		.catch((error)=> {
			if(!(error instanceof pig_core.error.PigError)) {
				error=new pig_core.error.PigError({
					error: error,
					message: "route.process()",
					statusCode: http.status.code.INTERNAL_SERVER_ERROR,
					stack: error.stack
				});
			}
			pig_core.log.error(error);
			throw error;
		});
};

/**
 * translates the operation request into an instance of a handler class
 * @param {IncommingRequest} incommingRequest
 * @returns {RouteHandler}
 * @throws {Error} if operation is not known
 * @private
 */
exports._requestToHandlers=function(incommingRequest) {
	return _.map(incommingRequest.extensions.operations, function(operation) {
		const service=services.getById(operation.serviceId);
		switch(service.class) {
			case constant.api.service.class.DATABASE: {
				return new database.Handler(incommingRequest, operation, service);
			}
			case constant.api.service.class.SERVICE: {
				if(service.type===constant.api.service.type.HTTP) {
					return new http.Handler(incommingRequest, operation, service);
				} else if(service.type===constant.api.service.type.TRANSFORM) {
					return new transform.Handler(incommingRequest, operation, service);
				}
			}
			// eslint-disable-next-line no-fallthrough
			default: {
				throw new pig_core.error.PigError({
					details: `class=${service.class}, type=${service.type}`,
					message: "unknown service class and/or type",
					statusCode: http.status.code.BAD_REQUEST
				});
			}
		}
	});
};
