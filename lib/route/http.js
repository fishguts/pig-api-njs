/**
 * User: curtis
 * Date: 2/14/18
 * Time: 11:51 AM
 * Copyright @2018 by Xraymen Inc.
 */

const {Handler}=require("./_base");
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const {HttpService}=require("../services/http");

/**
 * @type {module:pig-core/http}
 */
module.exports=Object.assign(module.exports, http);

/**
 * Attempts to perform services (http and friends)
 * @typedef {Handler} HttpRouteHandler
 */
class HttpRouteHandler extends Handler {
	/**
	 * Processes the route and returns a promise that is fulfilled with the service's result
	 * @returns {Promise<any>}
	 */
	process() {
		this.assertServiceType(HttpService);
		switch(this.operation.type) {
			case "execute":
			case "send": {
				return this.service.send(this.request);
			}
			default: {
				return Promise.reject(new PigError({
					instance: this,
					details: `unknown type '${this.operation.type}'`,
					message: "unknown service operation"
				}));
			}
		}
	}
}


exports.Handler=HttpRouteHandler;
